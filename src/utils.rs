// ----------------------------------------------------------------------------
use std::fs;
use std::path::{Path, PathBuf};

use bevy::prelude::info;
use png::{BitDepth, ColorType, Compression};
// ----------------------------------------------------------------------------
pub fn create_dir(path: PathBuf) -> Result<PathBuf, String> {
    if !path.exists() {
        fs::create_dir_all(&path).map_err(|e| format!("failed to create path: {}", e))?;
    } else if !path.is_dir() {
        return Err("provided path is not a directory".into());
    }

    path.canonicalize()
        .map_err(|e| format!("failed to expand to absolute path: {}", e))
}
// ----------------------------------------------------------------------------
pub fn check_img_file(
    path: &Path,
    expected_width: u32,
    expected_height: u32,
    color_type: ColorType,
    bitdepth: BitDepth,
    palette_size: Option<usize>,
) -> Result<(), String> {
    use std::fs::File;

    let decoder = png::Decoder::new(
        File::open(path).map_err(|e| format!("failed to open file {}: {}", path.display(), e))?,
    );
    let reader = decoder
        .read_info()
        .map_err(|e| format!("failed to read image info: {}", e))?;
    let info = reader.info();

    if expected_width != info.width || expected_height != info.height {
        Err(format!(
            "expected image size {}x{}. found: {}x{}",
            expected_width, expected_height, info.width, info.height
        ))
    } else if color_type != info.color_type {
        Err(format!(
            "expected image format {:?}. found: {:?}",
            color_type, info.color_type
        ))
    } else if bitdepth != info.bit_depth {
        Err(format!(
            "expected image bit depth {:?}. found: {:?}",
            bitdepth, info.bit_depth
        ))
    } else if let Some(palette) = info.palette.as_ref() {
        if palette.len() != palette_size.unwrap_or(0) * 3 {
            Err(format!(
                "expected image palette size {:?}. found: {:?}",
                palette_size,
                palette.len() / 3
            ))
        } else {
            Ok(())
        }
    } else {
        Ok(())
    }
}
// ----------------------------------------------------------------------------
#[allow(clippy::too_many_arguments)]
pub fn store_image(
    path: &Path,
    data: &[u8],
    width: u32,
    height: u32,
    colortype: ColorType,
    bitdepth: BitDepth,
    compression: Compression,
    palette: Option<&[u8]>,
) -> Result<(), String> {
    use std::fs::File;
    use std::io::BufWriter;

    info!("creating {}...", path.display());

    let mut outputdir = PathBuf::from(path);
    outputdir.pop();

    fs::create_dir_all(outputdir.as_path())
        .map_err(|why| format!("couldn't create outputdir {}: {}", outputdir.display(), why))?;

    let target = File::create(path).map_err(|e| format!("could create file: {}", e))?;

    let mut encoder = png::Encoder::new(BufWriter::new(target), width, height);
    encoder.set_color(colortype);
    if let ColorType::Indexed = colortype {
        encoder.set_palette(palette.expect("palette colors").to_vec());
    }
    encoder.set_depth(bitdepth);
    encoder.set_compression(compression);
    let mut writer = encoder
        .write_header()
        .map_err(|e| format!("failed to write image header: {}", e))?;

    writer
        .write_image_data(data)
        .map_err(|e| format!("failed to write image data: {}", e))
}
// ----------------------------------------------------------------------------
pub fn try_allocate_memory(size: usize, name: &str) -> Result<Vec<u8>, String> {
    let mut result = Vec::default();
    result
        .try_reserve_exact(size)
        .map_err(|e| format!("error allocating memory for {}: {}", name, e))?;
    result.resize(size, 0);

    Ok(result)
}
// ----------------------------------------------------------------------------
pub fn try_reserve_buffer(size: usize, item: &str) -> Result<Vec<u8>, String> {
    let mut result = Vec::default();

    result
        .try_reserve_exact(size)
        .map_err(|e| format!("error reserving memory for {}: {}", item, e))?;

    Ok(result)
}
// ----------------------------------------------------------------------------

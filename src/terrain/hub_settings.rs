// ----------------------------------------------------------------------------
use super::material_settings;
use super::{EnvDefinition, Witcher3HubId};
// ----------------------------------------------------------------------------
pub struct HubSettings {
    pub id: &'static str,
    pub name: &'static str,
    pub map_size: u32,
    pub terrain_size: f32,
    pub min_height: f32,
    pub max_height: f32,

    pub heightmap_file: String,
    pub texture_background_file: String,
    pub texture_overlay_file: String,
    pub texture_control_file: String,
    pub tintmap_file: String,

    pub materialset: material_settings::MaterialSet,
    pub environment: &'static str,
    pub clipmap_level: u8,
}
// ----------------------------------------------------------------------------
pub(super) fn prolog_village() -> HubSettings {
    let hubid = Witcher3HubId::Prologue;

    let terrain_size = 2000.0;
    let min_height = -37.0;
    let max_height = 45.0;
    let clipmap_level = 3;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size: hubid.map_size(),
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::prolog_village(),
        environment: EnvDefinition::PrologColorsSunset.path(),
        clipmap_level,
    }
}
// ----------------------------------------------------------------------------
pub(super) fn prolog_village_winter() -> HubSettings {
    let hubid = Witcher3HubId::PrologueWinter;

    let terrain_size = 2000.0;
    let min_height = -37.0;
    let max_height = 45.0;
    let clipmap_level = 3;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size: hubid.map_size(),
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::prolog_village_winter(),
        environment: EnvDefinition::WinterEpilog.path(),
        clipmap_level,
    }
}
// ----------------------------------------------------------------------------
pub(super) fn skellige() -> HubSettings {
    let hubid = Witcher3HubId::Skellige;

    let terrain_size = 7472.0;
    let min_height = -77.0;
    let max_height = 308.0;
    let clipmap_level = 5;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size: hubid.map_size(),
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::skellige(),
        environment: EnvDefinition::SkelligeBrown.path(),
        clipmap_level,
    }
}
// ------------------------------------------------------------------------
pub(super) fn novigrad() -> HubSettings {
    let hubid = Witcher3HubId::Novigrad;

    // since only 2^n size are supported this needs to be recalculated for a 16K rectangle
    let orig_map_size = 512 * 46;
    let orig_terrain_size = 8625.0;
    let resolution = orig_terrain_size / orig_map_size as f32;
    let map_size = hubid.map_size();
    let terrain_size = resolution * map_size as f32;
    let min_height = -65.0;
    let max_height = 295.0;
    let clipmap_level = 5;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size,
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::novigrad(),
        environment: EnvDefinition::NovigradSunset.path(),
        clipmap_level,
    }
}
// ------------------------------------------------------------------------
pub(super) fn velen() -> HubSettings {
    let hubid = Witcher3HubId::Velen;

    // since only 2^n size are supported this needs to be recalculated for a 16K rectangle
    let orig_map_size = 512 * 46;
    let orig_terrain_size = 8625.0;
    let resolution = orig_terrain_size / orig_map_size as f32;
    let map_size = hubid.map_size();
    let terrain_size = resolution * map_size as f32;
    let min_height = -65.0;
    let max_height = 295.0;
    let clipmap_level = 5;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size: hubid.map_size(),
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::novigrad(),
        environment: EnvDefinition::NovigradSunset.path(),
        clipmap_level,
    }
}
// ----------------------------------------------------------------------------
pub(super) fn kaer_morhen() -> HubSettings {
    let hubid = Witcher3HubId::KaerMorhen;

    let terrain_size = 8000.0;
    let min_height = -118.0;
    let max_height = 1682.0;
    let clipmap_level = 5;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size: hubid.map_size(),
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::kaer_morhen(),
        environment: EnvDefinition::KaerMorhenV09.path(),
        clipmap_level,
    }
}
// ------------------------------------------------------------------------
pub(super) fn wyzima_castle() -> HubSettings {
    let hubid = Witcher3HubId::Vizima;

    // since only 2^n size are supported this needs to be recalculated
    let orig_map_size = 512 * 9;
    let orig_terrain_size = 3600.0;
    let resolution = orig_terrain_size / orig_map_size as f32;
    let map_size = hubid.map_size();
    let terrain_size = resolution * map_size as f32;
    let min_height = -50.0;
    let max_height = 50.0;
    let clipmap_level = 3;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size,
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::wyzima_castle(),
        environment: EnvDefinition::Wyzima.path(),
        clipmap_level,
    }
}
// ------------------------------------------------------------------------
pub(super) fn isle_of_mist() -> HubSettings {
    let hubid = Witcher3HubId::IsleOfMist;

    let terrain_size = 1868.0;
    let min_height = -80.0;
    let max_height = 308.0;
    let clipmap_level = 3;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size: hubid.map_size(),
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::isle_of_mist(),
        environment: EnvDefinition::IsleOfMistDark.path(),
        clipmap_level,
    }
}
// ------------------------------------------------------------------------
pub(super) fn spiral_dark_valley() -> HubSettings {
    let hubid = Witcher3HubId::SpiralDarkValley;

    // since only 2^n size are supported this needs to be recalculated
    let orig_map_size = 1024 * 24;
    let orig_terrain_size = 8000.0;
    let resolution = orig_terrain_size / orig_map_size as f32;

    let map_size = hubid.map_size();
    let terrain_size = resolution * map_size as f32;
    let min_height = -100.0;
    let max_height = 600.0;
    let clipmap_level = 3;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size,
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::spiral(),
        environment: EnvDefinition::SpiralDarkValley.path(),
        clipmap_level,
    }
}
// ------------------------------------------------------------------------
pub(super) fn spiral_desert() -> HubSettings {
    let hubid = Witcher3HubId::SpiralDesert;

    // since only 2^n size are supported this needs to be recalculated
    let orig_map_size = 1024 * 24;
    let orig_terrain_size = 8000.0;
    let resolution = orig_terrain_size / orig_map_size as f32;

    let map_size = hubid.map_size();
    let terrain_size = resolution * map_size as f32;
    let min_height = -100.0;
    let max_height = 600.0;
    let clipmap_level = 4;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size,
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::spiral(),
        environment: EnvDefinition::SpiralDesertClear.path(),
        clipmap_level,
    }
}
// ------------------------------------------------------------------------
pub(super) fn spiral_elven_city() -> HubSettings {
    let hubid = Witcher3HubId::SpiralElvenCity;

    // since only 2^n size are supported this needs to be recalculated
    let orig_map_size = 1024 * 24;
    let orig_terrain_size = 8000.0;
    let resolution = orig_terrain_size / orig_map_size as f32;

    let map_size = hubid.map_size();
    let terrain_size = resolution * map_size as f32;
    let min_height = -100.0;
    let max_height = 600.0;
    let clipmap_level = 5;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size,
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::spiral(),
        environment: EnvDefinition::SpiralElvenCity.path(),
        clipmap_level,
    }
}
// ------------------------------------------------------------------------
pub(super) fn spiral_snow_valley() -> HubSettings {
    let hubid = Witcher3HubId::SpiralSnowValley;

    // since only 2^n size are supported this needs to be recalculated
    let orig_map_size = 1024 * 24;
    let orig_terrain_size = 8000.0;
    let resolution = orig_terrain_size / orig_map_size as f32;

    let map_size = hubid.map_size();
    let terrain_size = resolution * map_size as f32;
    let min_height = -100.0;
    let max_height = 600.0;
    let clipmap_level = 5;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size,
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::spiral(),
        environment: EnvDefinition::SpiralSnowHour.path(),
        clipmap_level,
    }
}
// ------------------------------------------------------------------------
pub(super) fn bob() -> HubSettings {
    let hubid = Witcher3HubId::Bob;

    let terrain_size = 8000.0;
    let min_height = -15.0;
    let max_height = 1785.0;
    let clipmap_level = 5;

    let mapdata = hubid.data_files();

    HubSettings {
        id: hubid.id(),
        name: hubid.caption(),
        map_size: hubid.map_size(),
        terrain_size,
        min_height,
        max_height,
        heightmap_file: mapdata.heightmap.filename().to_string(),
        texture_background_file: mapdata.texture_background.filename().to_string(),
        texture_overlay_file: mapdata.texture_overlay.filename().to_string(),
        texture_control_file: mapdata.texture_control.filename().to_string(),
        tintmap_file: mapdata.tintmap.filename().to_string(),
        materialset: material_settings::bob(),
        environment: EnvDefinition::SunnyBobV7.path(),
        clipmap_level,
    }
}
// ----------------------------------------------------------------------------

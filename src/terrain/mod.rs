// ----------------------------------------------------------------------------
use std::path::{Path, PathBuf};

use png::{BitDepth, ColorType};

use bevy::prelude::*;

use super::utils;

use super::environment::EnvDefinition;
use super::Witcher3HubId;
// ----------------------------------------------------------------------------
pub struct TerrainConfig {
    pub(super) terrain_dir: PathBuf,
    pub(super) texture_dir: PathBuf,
    available_hubs: Vec<Witcher3HubId>,
}
// ----------------------------------------------------------------------------
pub use self::hub_settings::HubSettings;
pub use self::material_settings::{MaterialSet, TerrainMaterialParam};
// ----------------------------------------------------------------------------
enum MapType {
    Heightmap(String),
    Texturing(String),
    TextureControl(String),
    Tintmap(String),
}
// ----------------------------------------------------------------------------
pub(super) struct TerrainDataFiles {
    heightmap: MapType,
    texture_background: MapType,
    texture_overlay: MapType,
    texture_control: MapType,
    tintmap: MapType,
}
// ----------------------------------------------------------------------------
mod hub_settings;
mod material_settings;
// ----------------------------------------------------------------------------
impl Witcher3HubId {
    // ------------------------------------------------------------------------
    pub const fn caption(&self) -> &'static str {
        match self {
            Witcher3HubId::Prologue => "🌍 Prolog (4K)",
            Witcher3HubId::PrologueWinter => "🌍 Prolog Winter (4K)",
            Witcher3HubId::Skellige => "🌍 Skellige Islands (16K)",
            Witcher3HubId::Novigrad => "🌍 Novigrad (16K)",
            Witcher3HubId::Velen => "🌍 Velen (16K)",
            Witcher3HubId::KaerMorhen => "🌍 Kaer Morhen (16K)",
            Witcher3HubId::Vizima => "🌍 Wyzima Castle (4K)",
            Witcher3HubId::IsleOfMist => "🌍 Isle of Mist (4K)",
            Witcher3HubId::SpiralDarkValley => "🌍 Spiral Dark Valley (4K)",
            Witcher3HubId::SpiralDesert => "🌍 Spiral Desert (8K)",
            Witcher3HubId::SpiralElvenCity => "🌍 Spiral Elven City (16K)",
            Witcher3HubId::SpiralSnowValley => "🌍 Spiral Snow Valley (16K)",
            Witcher3HubId::Bob => "🌍 Toussaint (16K)",
        }
    }
    // ------------------------------------------------------------------------
    pub const fn short_caption(&self) -> &'static str {
        match self {
            Witcher3HubId::Prologue => "🌍 Prolog",
            Witcher3HubId::PrologueWinter => "🌍 Prolog Winter",
            Witcher3HubId::Skellige => "🌍 Skellige",
            Witcher3HubId::Novigrad => "🌍 Novigrad",
            Witcher3HubId::Velen => "🌍 Velen",
            Witcher3HubId::KaerMorhen => "🌍 Kaer Morhen",
            Witcher3HubId::Vizima => "🌍 Wyzima",
            Witcher3HubId::IsleOfMist => "🌍 Isle of Mist",
            Witcher3HubId::SpiralDarkValley => "🌍 The Spiral",
            Witcher3HubId::SpiralDesert => "🌍 The Spiral",
            Witcher3HubId::SpiralElvenCity => "🌍 The Spiral",
            Witcher3HubId::SpiralSnowValley => "🌍 The Spiral",
            Witcher3HubId::Bob => "🌍 Toussaint",
        }
    }
    // ------------------------------------------------------------------------
    pub const fn textureset_caption(&self) -> &'static str {
        use Witcher3HubId::*;
        match self {
            Prologue => "🎨 Prolog",
            PrologueWinter => "🎨 Prolog Winter",
            Skellige | Vizima | IsleOfMist => "🎨 Skellige",
            Novigrad | Velen => "🎨 Novigrad",
            KaerMorhen => "🎨 Kaer Morhen",
            SpiralDarkValley | SpiralDesert | SpiralElvenCity | SpiralSnowValley => "🎨 The Spiral",
            Bob => "🎨 Toussaint",
        }
    }
    // ------------------------------------------------------------------------
    pub fn settings(&self) -> HubSettings {
        match self {
            Witcher3HubId::Prologue => hub_settings::prolog_village(),
            Witcher3HubId::PrologueWinter => hub_settings::prolog_village_winter(),
            Witcher3HubId::Skellige => hub_settings::skellige(),
            Witcher3HubId::Novigrad => hub_settings::novigrad(),
            Witcher3HubId::Velen => hub_settings::velen(),
            Witcher3HubId::KaerMorhen => hub_settings::kaer_morhen(),
            Witcher3HubId::Vizima => hub_settings::wyzima_castle(),
            Witcher3HubId::IsleOfMist => hub_settings::isle_of_mist(),
            Witcher3HubId::SpiralDarkValley => hub_settings::spiral_dark_valley(),
            Witcher3HubId::SpiralDesert => hub_settings::spiral_desert(),
            Witcher3HubId::SpiralElvenCity => hub_settings::spiral_elven_city(),
            Witcher3HubId::SpiralSnowValley => hub_settings::spiral_snow_valley(),
            Witcher3HubId::Bob => hub_settings::bob(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn materialset(&self) -> MaterialSet {
        match self {
            Witcher3HubId::Prologue => material_settings::prolog_village(),
            Witcher3HubId::PrologueWinter => material_settings::prolog_village_winter(),
            Witcher3HubId::Skellige => material_settings::skellige(),
            Witcher3HubId::Novigrad => material_settings::novigrad(),
            Witcher3HubId::Velen => material_settings::novigrad(),
            Witcher3HubId::KaerMorhen => material_settings::kaer_morhen(),
            Witcher3HubId::Vizima => material_settings::wyzima_castle(),
            Witcher3HubId::IsleOfMist => material_settings::isle_of_mist(),
            Witcher3HubId::SpiralDarkValley => material_settings::spiral(),
            Witcher3HubId::SpiralDesert => material_settings::spiral(),
            Witcher3HubId::SpiralElvenCity => material_settings::spiral(),
            Witcher3HubId::SpiralSnowValley => material_settings::spiral(),
            Witcher3HubId::Bob => material_settings::bob(),
        }
    }
    // ------------------------------------------------------------------------
    pub const fn map_size(&self) -> u32 {
        match self {
            Witcher3HubId::Prologue => 256 * 16,
            Witcher3HubId::PrologueWinter => 256 * 16,
            Witcher3HubId::Skellige => 512 * 32,
            Witcher3HubId::Novigrad => 16384,
            Witcher3HubId::Velen => 16384,
            Witcher3HubId::KaerMorhen => 1024 * 16,
            Witcher3HubId::Vizima => 4096,
            Witcher3HubId::IsleOfMist => 1024 * 4,
            Witcher3HubId::SpiralDarkValley => 4096,
            Witcher3HubId::SpiralDesert => 8192,
            Witcher3HubId::SpiralElvenCity => 16384,
            Witcher3HubId::SpiralSnowValley => 16384,
            Witcher3HubId::Bob => 512 * 32,
        }
    }
    // ------------------------------------------------------------------------
    pub const fn terrain_texture_count(&self) -> u8 {
        use Witcher3HubId::*;
        match self {
            Prologue | PrologueWinter => 31,
            Skellige | Vizima | IsleOfMist => 31,
            Novigrad | Velen => 31,
            KaerMorhen => 20,
            SpiralDarkValley | SpiralDesert | SpiralElvenCity | SpiralSnowValley => 25,
            Bob => 31,
        }
    }
    // ------------------------------------------------------------------------
    pub const fn id(&self) -> &'static str {
        match self {
            Witcher3HubId::Prologue => "prolog_village",
            Witcher3HubId::PrologueWinter => "prolog_village_winter",
            Witcher3HubId::Skellige => "skellige",
            Witcher3HubId::Novigrad => "novigrad",
            Witcher3HubId::Velen => "velen",
            Witcher3HubId::KaerMorhen => "kaer_morhen",
            Witcher3HubId::Vizima => "wyzima_castle",
            Witcher3HubId::IsleOfMist => "isle_of_mist",
            Witcher3HubId::SpiralDarkValley => "spiral.dark_valley",
            Witcher3HubId::SpiralDesert => "spiral.desert",
            Witcher3HubId::SpiralElvenCity => "spiral.elven_city",
            Witcher3HubId::SpiralSnowValley => "spiral.snow_valley",
            Witcher3HubId::Bob => "bob",
        }
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn heightmap_filename(&self) -> String {
        format!("{}.heightmap.png", self.id())
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn texturemap_background_filename(&self) -> String {
        format!("{}.bkgrnd.png", self.id())
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn texturemap_overlay_filename(&self) -> String {
        format!("{}.overlay.png", self.id())
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn texturemap_control_filename(&self) -> String {
        format!("{}.blendcontrol.png", self.id())
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn tintmap_filename(&self) -> String {
        format!("{}.tint.png", self.id())
    }
    // ------------------------------------------------------------------------
    fn data_files(&self) -> TerrainDataFiles {
        TerrainDataFiles {
            heightmap: MapType::Heightmap(self.heightmap_filename()),
            texture_background: MapType::Texturing(self.texturemap_background_filename()),
            texture_overlay: MapType::Texturing(self.texturemap_overlay_filename()),
            texture_control: MapType::TextureControl(self.texturemap_control_filename()),
            tintmap: MapType::Tintmap(self.tintmap_filename()),
        }
    }
    // ------------------------------------------------------------------------
    pub const fn materialset_path(&self) -> &str {
        match self {
            Witcher3HubId::Prologue => "levels/prolog_village/terrain_material.w2mg",
            Witcher3HubId::PrologueWinter => "levels/prolog_village_winter/terrain_material.w2mg",
            Witcher3HubId::Skellige
            | Witcher3HubId::Vizima
            | Witcher3HubId::IsleOfMist  => "levels/skellige/skellige_terrain_material.w2mg",
            Witcher3HubId::Novigrad
            | Witcher3HubId::Velen => "levels/novigrad/terrain_material.w2mg",
            Witcher3HubId::KaerMorhen => "levels/kaer_morhen/terrain_material.w2mg",
            Witcher3HubId::SpiralDarkValley
            | Witcher3HubId::SpiralDesert
            | Witcher3HubId::SpiralElvenCity
            | Witcher3HubId::SpiralSnowValley => "levels/the_spiral/terrain_material_spiral.w2mg",
            Witcher3HubId::Bob => "dlc/bob/data/levels/bob/bob_terrain_material.w2mg",
        }
    }
    // ------------------------------------------------------------------------
    #[rustfmt::skip]
    #[inline(always)]
    pub(crate) fn material_path_diffuse(&self, slot: u8, extension: &str) -> String {
        use Witcher3HubId::*;

        match self {
            Prologue => format!(
                "levels/prolog_village/prolog_village.texarray.texture_{}.{}", slot, extension
            ),
            PrologueWinter => format!(
                "levels/prolog_village_winter/prolog_village.texarray.texture_{}.{}", slot, extension
            ),
            Skellige | IsleOfMist | Vizima => format!(
                "levels/skellige/skellige_terrain_atlas.texarray.texture_{}.{}", slot, extension
            ),
            Novigrad | Velen => format!(
                "levels/novigrad/novigrad_array.texarray.texture_{}.{}", slot, extension
            ),
            KaerMorhen => format!(
                "levels/kaer_morhen/kaer_morhen_valley.texarray.texture_{}.{}", slot, extension
            ),
            SpiralDarkValley | SpiralDesert | SpiralElvenCity | SpiralSnowValley => format!(
                "levels/the_spiral/spiral_terrain_array.texarray.texture_{}.{}", slot, extension
            ),
            Bob => format!(
                "dlc/bob/data/levels/bob/bob_array.texarray.texture_{}.{}", slot, extension
            ),
        }
    }
    // ------------------------------------------------------------------------
    #[rustfmt::skip]
    #[inline(always)]
    pub(crate) fn material_path_normal(&self, slot: u8, extension: &str) -> String {
        use Witcher3HubId::*;

        match self {
            Prologue => format!(
                "levels/prolog_village/prolog_village_normals.texarray.texture_{}.{}", slot, extension
            ),
            PrologueWinter => format!(
                "levels/prolog_village_winter/prolog_village_normals.texarray.texture_{}.{}", slot, extension
            ),
            Skellige | IsleOfMist | Vizima => format!(
                "levels/skellige/skellige_terrain_normal_atlas.texarray.texture_{}.{}", slot, extension
            ),
            Novigrad | Velen => format!(
                "levels/novigrad/novigrad_array_n.texarray.texture_{}.{}", slot, extension
            ),
            KaerMorhen => format!(
                "levels/kaer_morhen/kaer_morhen_valley_normals.texarray.texture_{}.{}", slot, extension
            ),
            SpiralDarkValley | SpiralDesert | SpiralElvenCity | SpiralSnowValley => format!(
                "levels/the_spiral/spiral_terrain_array_n.texarray.texture_{}.{}", slot, extension
            ),
            Bob => format!(
                "dlc/bob/data/levels/bob/bob_array_n.texarray.texture_{}.{}", slot, extension
            ),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TerrainConfig {
    // ------------------------------------------------------------------------
    pub fn available_hubdata(&self) -> &[Witcher3HubId] {
        &self.available_hubs
    }
    // ------------------------------------------------------------------------
    pub fn terrain_dir(&self) -> &Path {
        &self.terrain_dir
    }
    // ------------------------------------------------------------------------
    pub fn texture_dir(&self) -> &Path {
        &self.texture_dir
    }
    // ------------------------------------------------------------------------
    pub(super) fn check_available_hub_data(&mut self) {
        use Witcher3HubId::*;

        let mut result = Vec::new();

        info!(
            "checking for extracted witcher 3 vanilla terrain data in {}...",
            self.terrain_dir.display()
        );
        for hubid in [
            Prologue,
            PrologueWinter,
            Skellige,
            Novigrad,
            Velen,
            KaerMorhen,
            Vizima,
            IsleOfMist,
            SpiralDarkValley,
            SpiralDesert,
            SpiralElvenCity,
            SpiralSnowValley,
            Bob,
        ] {
            debug!("> searching for {} data...", hubid.caption());

            let map_size = hubid.map_size();
            let files = hubid.data_files();

            let files = [
                &files.heightmap,
                &files.texture_background,
                &files.texture_overlay,
                &files.texture_control,
                &files.tintmap,
            ];

            if files
                .iter()
                .map(|datafile| check_map_file(&self.terrain_dir, datafile, map_size))
                .collect::<Option<Vec<_>>>()
                .is_none()
            {
                continue;
            }

            info!("found {}", hubid.caption());
            result.push(hubid);
        }

        self.available_hubs = result;
    }
    // ------------------------------------------------------------------------
    pub(super) fn validate(mut self) -> Result<Self, String> {
        self.terrain_dir = utils::create_dir(self.terrain_dir)?;
        self.texture_dir = utils::create_dir(self.texture_dir)?;
        Ok(self)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl MapType {
    // ------------------------------------------------------------------------
    fn filename(&self) -> &str {
        match self {
            MapType::Heightmap(filename) => filename,
            MapType::Texturing(filename) => filename,
            MapType::TextureControl(filename) => filename,
            MapType::Tintmap(filename) => filename,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for TerrainConfig {
    // ------------------------------------------------------------------------
    fn default() -> Self {
        Self {
            terrain_dir: PathBuf::from("./w3.terrain.new/"),
            texture_dir: PathBuf::from("./w3.textures.new/"),
            available_hubs: Vec::default(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn check_map_file(base_path: &Path, datafile: &MapType, expected_size: u32) -> Option<()> {
    use BitDepth::*;
    use ColorType::*;

    let (filename, color_type, bitdepth, palette_size) = match datafile {
        MapType::Heightmap(file) => (file, Grayscale, Sixteen, None),
        MapType::Texturing(file) => (file, Indexed, Eight, Some(32)),
        MapType::TextureControl(file) => (file, Indexed, Eight, Some(64)),
        MapType::Tintmap(file) => (file, Rgba, Eight, None),
    };

    let path = base_path.join(filename);
    if path.is_file() {
        match utils::check_img_file(
            &path,
            expected_size,
            expected_size,
            color_type,
            bitdepth,
            palette_size,
        ) {
            Ok(_) => {
                debug!(">> {}: found.", filename);
                Some(())
            }
            Err(e) => {
                warn!(">> {}: {}", filename, e);
                None
            }
        }
    } else {
        debug!(">> {}: missing.", filename);
        None
    }
}
// ----------------------------------------------------------------------------

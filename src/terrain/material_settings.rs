// ----------------------------------------------------------------------------
use crate::Witcher3HubId;
use crate::Witcher3HubId::*;
// ----------------------------------------------------------------------------
#[derive(Clone, Copy)]
pub struct TerrainMaterialParam {
    pub blend_sharpness: f32,
    pub slope_base_dampening: f32,
    pub slope_normal_dampening: f32,
    pub specularity: f32,
    pub specularity_base: f32,
    pub specularity_scale: f32,
    pub falloff: f32,
}
// ----------------------------------------------------------------------------
pub struct MaterialSet {
    /// path to base materialset (w2mg)
    pub path: String,
    /// path to diffuse textures (size and type verified)
    pub diffuse: Vec<String>,
    /// path to normal textures (size and type verified)
    pub normal: Vec<String>,
    /// materialsettings
    pub parameter: Vec<TerrainMaterialParam>,
}
// ----------------------------------------------------------------------------
pub fn prolog_village() -> MaterialSet {
    create_material_set_config(Prologue, "png", prolog_material_params().to_vec())
}
// ----------------------------------------------------------------------------
#[rustfmt::skip]
pub fn prolog_village_winter() -> MaterialSet {
    create_material_set_config(PrologueWinter, "png", prolog_winter_material_params().to_vec())
}
// ----------------------------------------------------------------------------
pub fn skellige() -> MaterialSet {
    create_material_set_config(Skellige, "png", skellige_material_params().to_vec())
}
// ----------------------------------------------------------------------------
pub fn novigrad() -> MaterialSet {
    create_material_set_config(Novigrad, "png", novigrad_material_params().to_vec())
}
// ----------------------------------------------------------------------------
pub fn kaer_morhen() -> MaterialSet {
    create_material_set_config(KaerMorhen, "png", kaer_morhen_material_params().to_vec())
}
// ----------------------------------------------------------------------------
pub fn wyzima_castle() -> MaterialSet {
    create_material_set_config(Vizima, "png", wyzima_castle_material_params().to_vec())
}
// ----------------------------------------------------------------------------
pub fn isle_of_mist() -> MaterialSet {
    create_material_set_config(IsleOfMist, "png", isle_of_mist_material_params().to_vec())
}
// ----------------------------------------------------------------------------
pub fn spiral() -> MaterialSet {
    create_material_set_config(SpiralDesert, "png", spiral_material_params().to_vec())
}
// ----------------------------------------------------------------------------
pub fn bob() -> MaterialSet {
    create_material_set_config(Bob, "png", bob_material_params().to_vec())
}
// ----------------------------------------------------------------------------
impl Default for TerrainMaterialParam {
    // ------------------------------------------------------------------------
    fn default() -> Self {
        Self {
            blend_sharpness: 0.5,
            slope_base_dampening: 0.5,
            slope_normal_dampening: 0.5,
            specularity: 0.0,
            specularity_base: 0.0,
            specularity_scale: 0.0,
            falloff: 0.0,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn create_material_set_config(
    hubid: Witcher3HubId,
    texture_ext: &str,
    material_params: Vec<TerrainMaterialParam>,
) -> MaterialSet {
    let diffuse = (0..hubid.terrain_texture_count())
        .map(|i| hubid.material_path_diffuse(i, texture_ext))
        .collect::<Vec<_>>();

    let normal = (0..hubid.terrain_texture_count())
        .map(|i| hubid.material_path_normal(i, texture_ext))
        .collect::<Vec<_>>();

    MaterialSet {
        path: hubid.materialset_path().to_string(),
        diffuse,
        normal,
        parameter: material_params,
    }
}
// ----------------------------------------------------------------------------
#[allow(clippy::excessive_precision)]
fn prolog_material_params() -> [TerrainMaterialParam; 31] {
    [
        // 1
        TerrainMaterialParam {
            blend_sharpness: 0.3650000095,
            specularity_scale: 0.3225809932,
            specularity: 0.224999994,
            specularity_base: 0.5161290169,
            ..Default::default()
        },
        // 2
        TerrainMaterialParam {
            blend_sharpness: 0.163635999,
            specularity: 0.7170000076,
            specularity_base: 0.5279999971,
            ..Default::default()
        },
        // 3
        TerrainMaterialParam {
            blend_sharpness: 0.2060610056,
            slope_normal_dampening: 0.0121210003,
            specularity: 0.2460000068,
            specularity_base: 0.5360000134,
            ..Default::default()
        },
        // 4
        TerrainMaterialParam {
            blend_sharpness: 0.3220340014,
            slope_base_dampening: 0.2711859941,
            slope_normal_dampening: 0.4848479927,
            specularity: 0.2630000114,
            specularity_base: 0.5429999828,
            ..Default::default()
        },
        // 5
        TerrainMaterialParam {
            blend_sharpness: 0.1557790041,
            specularity: 0.0542169996,
            specularity_base: 0.566264987,
            ..Default::default()
        },
        // --- 6
        TerrainMaterialParam {
            blend_sharpness: 0.1700000018,
            specularity: 0.0903609991,
            specularity_base: 0.566264987,
            specularity_scale: 0.0160000008,
            ..Default::default()
        },
        // 7
        TerrainMaterialParam {
            specularity: 0.4169999957,
            specularity_base: 0.5099999905,
            specularity_scale: 0.3225809932,
            ..Default::default()
        },
        // 8
        TerrainMaterialParam {
            blend_sharpness: 0.5921049714,
            slope_base_dampening: 0.5789470077,
            specularity: 0.52700001,
            specularity_base: 0.5120000243,
            specularity_scale: 0.0160000008,
            ..Default::default()
        },
        // 9
        TerrainMaterialParam {
            specularity: 0.3870970011,
            specularity_base: 0.5322579741,
            ..Default::default()
        },
        // 10
        TerrainMaterialParam {
            blend_sharpness: 0.1368419975,
            specularity: 0.224999994,
            specularity_base: 0.4779999852,
            specularity_scale: 0.1700000018,
            ..Default::default()
        },
        // --- 11
        TerrainMaterialParam {
            blend_sharpness: 0.1684210002,
            slope_base_dampening: 0.1894740015,
            specularity: 0.3870970011,
            specularity_base: 0.5645160079,
            specularity_scale: 0.903226018,
            ..Default::default()
        },
        // 12
        TerrainMaterialParam {
            blend_sharpness: 0.5368419886,
            slope_base_dampening: 0.400000006,
            specularity: 0.370968014,
            specularity_base: 0.596773982,
            specularity_scale: 0.8548390269,
            ..Default::default()
        },
        // 13
        TerrainMaterialParam {
            blend_sharpness: 0.1789470017,
            specularity: 0.4609999955,
            specularity_base: 0.5210000277,
            ..Default::default()
        },
        // 14
        TerrainMaterialParam {
            blend_sharpness: 0.3644070029,
            slope_base_dampening: 0.3644070029,
            specularity: 0.4838710129,
            specularity_base: 0.548386991,
            ..Default::default()
        },
        // 15
        TerrainMaterialParam {
            blend_sharpness: 0.2150000036,
            specularity: 0.351000011,
            specularity_base: 0.4889999926,
            specularity_scale: 0.0869999975,
            ..Default::default()
        },
        // --- 16
        TerrainMaterialParam {
            slope_normal_dampening: 0.2181819975,
            specularity: 0.4230000079,
            specularity_base: 0.3619999886,
            specularity_scale: 0.1640000045,
            ..Default::default()
        },
        // 17
        TerrainMaterialParam {
            blend_sharpness: 0.1199999973,
            specularity: 0.3790000081,
            specularity_base: 0.5490000248,
            ..Default::default()
        },
        // 18
        TerrainMaterialParam {
            blend_sharpness: 0.1757580042,
            slope_base_dampening: 0.9878789783,
            specularity: 0.5161290169,
            specularity_base: 0.5645160079,
            ..Default::default()
        },
        // 19
        TerrainMaterialParam {
            blend_sharpness: 0.1299999952,
            specularity: 0.4720000029,
            specularity_base: 0.5870000124,
            specularity_scale: 0.0049999999,
            ..Default::default()
        },
        // 20
        TerrainMaterialParam {
            blend_sharpness: 0.1052630022,
            slope_base_dampening: 0.1016950011,
            specularity: 0.1199999973,
            specularity_base: 0.5870000124,
            ..Default::default()
        },
        // --- 21
        TerrainMaterialParam {
            specularity: 0.1612900048,
            specularity_base: 0.419355005,
            specularity_scale: 0.7741940022,
            ..Default::default()
        },
        // 22
        TerrainMaterialParam {
            specularity: 0.3449999988,
            specularity_base: 0.5640000105,
            ..Default::default()
        },
        // 23
        TerrainMaterialParam {
            specularity: 0.4169999957,
            specularity_base: 0.5490000248,
            ..Default::default()
        },
        // 24
        TerrainMaterialParam {
            ..Default::default()
        },
        // 25
        TerrainMaterialParam {
            blend_sharpness: 0.4322029948,
            slope_base_dampening: 0.4067800045,
            specularity: 0.3619999886,
            specularity_base: 0.5149999857,
            ..Default::default()
        },
        // --- 26
        TerrainMaterialParam {
            blend_sharpness: 0.174999997,
            specularity: 0.370968014,
            specularity_base: 0.5645160079,
            falloff: 0.0549999997,
            ..Default::default()
        },
        // 27
        TerrainMaterialParam {
            blend_sharpness: 0.3449999988,
            specularity: 0.419355005,
            specularity_base: 0.4838710129,
            falloff: 0.3620690107,
            specularity_scale: 0.2586210072,
            ..Default::default()
        },
        // 28
        TerrainMaterialParam {
            blend_sharpness: 0.1319440007,
            specularity: 0.3680000007,
            specularity_base: 0.5049999952,
            ..Default::default()
        },
        // 29
        TerrainMaterialParam {
            specularity_base: 0.5049999952,
            ..Default::default()
        },
        // 30
        TerrainMaterialParam {
            specularity: 0.4449999928,
            specularity_base: 0.4779999852,
            specularity_scale: 0.4720000029,
            ..Default::default()
        },
        // --- 31
        TerrainMaterialParam {
            specularity: 0.4889999926,
            specularity_base: 0.5490000248,
            ..Default::default()
        },
    ]
}
// ----------------------------------------------------------------------------
#[allow(clippy::excessive_precision)]
fn prolog_winter_material_params() -> [TerrainMaterialParam; 31] {
    [
        // 1
        TerrainMaterialParam {
            blend_sharpness: 0.1585370004,
            specularity_scale: 0.146340996,
            specularity: 0.4635419846,
            specularity_base: 0.5548779964,
            falloff: 0.3780489862,
            ..Default::default()
        },
        // 2
        TerrainMaterialParam::default(),
        // 3
        TerrainMaterialParam::default(),
        // 4
        TerrainMaterialParam::default(),
        // 5
        TerrainMaterialParam::default(),
        // 6
        TerrainMaterialParam::default(),
        // 7
        TerrainMaterialParam {
            specularity_scale: 0.140625,
            specularity: 0.3229169846,
            specularity_base: 0.1770830005,
            ..Default::default()
        },
        // 8
        TerrainMaterialParam::default(),
        // 9
        TerrainMaterialParam::default(),
        // 10
        TerrainMaterialParam::default(),
        // 11
        TerrainMaterialParam {
            specularity: 0.1302080005,
            specularity_base: 0.5416669846,
            falloff: 0.03125,
            ..Default::default()
        },
        // 12
        TerrainMaterialParam {
            blend_sharpness: 0.0975610018,
            specularity_scale: 0.9379310012,
            specularity: 0.5793099999,
            specularity_base: 0.6689659953,
            ..Default::default()
        },
        // 13
        TerrainMaterialParam::default(),
        // 14
        TerrainMaterialParam::default(),
        // 15
        TerrainMaterialParam::default(),
        // 16
        TerrainMaterialParam::default(),
        // 17
        TerrainMaterialParam {
            specularity_scale: 0.21875,
            specularity: 0.4010419846,
            specularity_base: 0.5104169846,
            ..Default::default()
        },
        // 18
        TerrainMaterialParam::default(),
        // 19
        TerrainMaterialParam {
            blend_sharpness: 0.0670730025,
            ..Default::default()
        },
        // 20
        TerrainMaterialParam::default(),
        // 21
        TerrainMaterialParam::default(),
        // 22
        TerrainMaterialParam::default(),
        // 23
        TerrainMaterialParam::default(),
        // 24
        TerrainMaterialParam::default(),
        // 25
        TerrainMaterialParam::default(),
        // 26
        TerrainMaterialParam::default(),
        // 27
        TerrainMaterialParam::default(),
        // 28
        TerrainMaterialParam::default(),
        // 29
        TerrainMaterialParam::default(),
        // 30
        TerrainMaterialParam::default(),
        // 31
        TerrainMaterialParam::default(),
    ]
}
// ----------------------------------------------------------------------------
#[allow(clippy::excessive_precision)]
fn skellige_material_params() -> [TerrainMaterialParam; 31] {
    [
        // 1
        TerrainMaterialParam {
            blend_sharpness: 0.317241,
            slope_base_dampening: 0.211409,
            slope_normal_dampening: 0.268456,
            specularity_scale: 0.474227,
            specularity: 0.463918,
            specularity_base: 0.474,
            falloff: 0.088942,
        },
        // 2
        TerrainMaterialParam {
            blend_sharpness: 0.252841,
            slope_base_dampening: 0.903409,
            slope_normal_dampening: 0.536932,
            specularity_scale: 0.0,
            specularity: 0.403141,
            specularity_base: 0.539267,
            falloff: 0.289231,
        },
        // 3
        TerrainMaterialParam {
            blend_sharpness: 0.270492,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.515924,
            specularity_base: 0.525773,
            falloff: 0.5,
        },
        // 4
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.579618,
            specularity_base: 0.515,
            falloff: 0.0,
        },
        // 5
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.412371,
            specularity: 0.443299,
            specularity_base: 0.453,
            falloff: 0.07947,
        },
        // 6
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.515464,
            specularity_base: 0.556701,
            falloff: 0.0,
        },
        // 7
        TerrainMaterialParam {
            blend_sharpness: 0.205357,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.639175,
            specularity: 0.340206,
            specularity_base: 0.453608,
            falloff: 0.949519,
        },
        // 8
        TerrainMaterialParam {
            blend_sharpness: 0.341667,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.041237,
            specularity: 0.494845,
            specularity_base: 0.505155,
            falloff: 0.147436,
        },
        // 9
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.882813,
            slope_normal_dampening: 0.867188,
            specularity_scale: 0.0,
            specularity: 0.494845,
            specularity_base: 0.534031,
            falloff: 0.627404,
        },
        // 10
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.451613,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.391753,
            specularity_base: 0.549738,
            falloff: 0.0,
        },
        // 11
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.4,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.402062,
            specularity_base: 0.515464,
            falloff: 0.0,
        },
        // 12
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.360825,
            specularity: 0.350515,
            specularity_base: 0.402062,
            falloff: 1.0,
        },
        // 13
        TerrainMaterialParam {
            blend_sharpness: 0.247423,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.434426,
            specularity_scale: 0.0,
            specularity: 0.814433,
            specularity_base: 0.536,
            falloff: 0.516827,
        },
        // 14
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.233333,
            slope_normal_dampening: 1.0,
            specularity_scale: 0.0,
            specularity: 0.412371,
            specularity_base: 0.680412,
            falloff: 0.608247,
        },
        // 15
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.585938,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.134021,
            specularity: 0.391753,
            specularity_base: 0.463918,
            falloff: 0.0,
        },
        // 16
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.154639,
            specularity: 0.381443,
            specularity_base: 0.443299,
            falloff: 0.0,
        },
        // 17
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.408602,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.690722,
            specularity: 0.154639,
            specularity_base: 0.453608,
            falloff: 0.56701,
        },
        // 18
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.28866,
            specularity_base: 0.546,
            falloff: 0.639423,
        },
        // 19
        TerrainMaterialParam {
            blend_sharpness: 0.254098,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.608247,
            specularity: 0.42268,
            specularity_base: 0.42268,
            falloff: 0.4375,
        },
        // 20
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.4,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.391753,
            specularity: 0.195876,
            specularity_base: 0.505155,
            falloff: 0.4,
        },
        // 21
        TerrainMaterialParam {
            blend_sharpness: 0.482143,
            slope_base_dampening: 0.339286,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.670103,
            specularity: 0.876289,
            specularity_base: 0.494845,
            falloff: 0.413462,
        },
        // 22
        TerrainMaterialParam {
            blend_sharpness: 0.1875,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.690722,
            specularity_base: 0.505155,
            falloff: 0.642857,
        },
        // 23
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.4,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.587629,
            specularity_base: 0.515464,
            falloff: 0.7,
        },
        // 24
        TerrainMaterialParam {
            blend_sharpness: 0.196429,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.453608,
            specularity_base: 0.52,
            falloff: 1.0,
        },
        // 25
        TerrainMaterialParam {
            blend_sharpness: 0.373016,
            slope_base_dampening: 0.071429,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.43299,
            specularity_base: 0.536082,
            falloff: 0.6,
        },
        // 26
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.772152,
            slope_normal_dampening: 0.848101,
            specularity_scale: 0.0,
            specularity: 0.42268,
            specularity_base: 0.546392,
            falloff: 0.320513,
        },
        // 27
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.867089,
            slope_normal_dampening: 0.246835,
            specularity_scale: 0.0,
            specularity: 0.484536,
            specularity_base: 0.556701,
            falloff: 0.199519,
        },
        // 28
        TerrainMaterialParam {
            blend_sharpness: 0.324094,
            slope_base_dampening: 0.208955,
            slope_normal_dampening: 0.266525,
            specularity_scale: 0.0,
            specularity: 0.402062,
            specularity_base: 0.587629,
            falloff: 0.093817,
        },
        // 29
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.4,
            slope_normal_dampening: 0.032767,
            specularity_scale: 0.0,
            specularity: 0.34,
            specularity_base: 0.531,
            falloff: 0.0,
        },
        // 30
        TerrainMaterialParam {
            blend_sharpness: 0.3,
            slope_base_dampening: 0.4,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.34,
            specularity_base: 0.531,
            falloff: 0.0,
        },
        // 31
        TerrainMaterialParam {
            blend_sharpness: 0.121495,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 1.0,
            specularity_scale: 0.934579,
            specularity: 0.897196,
            specularity_base: 0.278351,
            falloff: 0.0,
        },
    ]
}
// ----------------------------------------------------------------------------
#[allow(clippy::excessive_precision)]
fn novigrad_material_params() -> [TerrainMaterialParam; 31] {
    [
        // 1
        TerrainMaterialParam {
            blend_sharpness: 0.143939,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.17033,
            specularity: 0.225275,
            specularity_base: 0.478022,
            falloff: 0.117949,
        },
        // 2
        TerrainMaterialParam {
            blend_sharpness: 0.0,
            slope_base_dampening: 0.856061,
            slope_normal_dampening: 0.340909,
            specularity_scale: 0.0,
            specularity: 0.311594,
            specularity_base: 0.514493,
            falloff: 0.145455,
        },
        // 3
        TerrainMaterialParam {
            specularity: 0.456522,
            specularity_base: 0.507246,
            falloff: 0.390805,
            ..Default::default()
        },
        // 4
        TerrainMaterialParam {
            blend_sharpness: 0.022727,
            slope_base_dampening: 0.136364,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.235849,
            specularity: 0.216981,
            specularity_base: 0.490566,
            falloff: 0.415152,
        },
        // 5
        TerrainMaterialParam {
            blend_sharpness: 0.159091,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.02,
            specularity_scale: 0.016484,
            specularity: 0.527473,
            specularity_base: 0.521,
            falloff: 0.0,
        },
        // 6
        TerrainMaterialParam {
            blend_sharpness: 0.323232,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.246377,
            specularity_base: 0.536232,
            falloff: 0.121212,
        },
        // 7
        TerrainMaterialParam {
            blend_sharpness: 0.121212,
            slope_base_dampening: 0.440789,
            slope_normal_dampening: 0.774775,
            specularity_scale: 0.0,
            specularity: 0.263736,
            specularity_base: 0.543956,
            falloff: 0.454955,
        },
        // 8
        TerrainMaterialParam {
            blend_sharpness: 0.181818,
            slope_base_dampening: 0.427632,
            slope_normal_dampening: 0.447368,
            specularity_scale: 0.0,
            specularity: 0.417582,
            specularity_base: 0.510989,
            falloff: 0.436782,
        },
        // 9
        TerrainMaterialParam {
            blend_sharpness: 0.030303,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.362637,
            specularity_base: 0.516,
            falloff: 0.497436,
        },
        // 10
        TerrainMaterialParam {
            blend_sharpness: 0.0,
            slope_base_dampening: 0.090909,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.549451,
            specularity_base: 0.527473,
            falloff: 0.0,
        },
        // 11
        TerrainMaterialParam {
            blend_sharpness: 0.466667,
            slope_base_dampening: 0.033333,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.505495,
            specularity_base: 0.538462,
            falloff: 0.0,
        },
        // 12
        TerrainMaterialParam {
            blend_sharpness: 0.083333,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.406593,
            specularity_base: 0.527473,
            falloff: 0.0,
        },
        // 13
        TerrainMaterialParam {
            blend_sharpness: 0.0,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.016484,
            specularity: 0.252747,
            specularity_base: 0.593407,
            falloff: 0.0,
        },
        // 14
        TerrainMaterialParam {
            blend_sharpness: 0.097403,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.379121,
            specularity_base: 0.549451,
            falloff: 0.045455,
        },
        // 15
        TerrainMaterialParam {
            blend_sharpness: 0.198276,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.005495,
            specularity: 0.472527,
            specularity_base: 0.587912,
            falloff: 0.19697,
        },
        // 16
        TerrainMaterialParam {
            blend_sharpness: 0.02459,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.461538,
            specularity_base: 0.521978,
            falloff: 0.098485,
        },
        // 17
        TerrainMaterialParam {
            blend_sharpness: 0.015326,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.120879,
            specularity_base: 0.587912,
            falloff: 0.188525,
        },
        // 18
        TerrainMaterialParam {
            blend_sharpness: 0.0,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.164835,
            specularity: 0.423077,
            specularity_base: 0.362637,
            falloff: 0.264901,
        },
        // 19
        TerrainMaterialParam {
            specularity: 0.3,
            specularity_base: 0.515,
            falloff: 0.5,
            ..Default::default()
        },
        // 20
        TerrainMaterialParam {
            specularity: 0.417582,
            specularity_base: 0.549451,
            falloff: 0.0,
            ..Default::default()
        },
        // 21
        TerrainMaterialParam {
            blend_sharpness: 0.0,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.142857,
            specularity: 0.302198,
            specularity_base: 0.587912,
            falloff: 0.0,
        },
        // 22
        TerrainMaterialParam {
            blend_sharpness: 0.0,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.087912,
            specularity: 0.351648,
            specularity_base: 0.489011,
            falloff: 0.248366,
        },
        // 23
        TerrainMaterialParam {
            blend_sharpness: 0.25,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.489,
            specularity_base: 0.538462,
            falloff: 0.0,
        },
        // 24
        TerrainMaterialParam {
            blend_sharpness: 0.567164,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.472527,
            specularity: 0.445055,
            specularity_base: 0.478,
            falloff: 0.1,
        },
        // 25
        TerrainMaterialParam {
            specularity: 0.489011,
            specularity_base: 0.549451,
            falloff: 0.0,
            ..Default::default()
        },
        // 26
        TerrainMaterialParam {
            blend_sharpness: 0.559701,
            slope_base_dampening: 0.431818,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.296703,
            specularity_base: 0.527,
            falloff: 0.067164,
        },
        // 27
        TerrainMaterialParam {
            blend_sharpness: 0.649254,
            slope_base_dampening: 0.363636,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.368132,
            specularity_base: 0.505,
            falloff: 0.059701,
        },
        // 28
        TerrainMaterialParam {
            specularity: 0.368,
            specularity_base: 0.505,
            falloff: 0.0,
            ..Default::default()
        },
        // 29
        TerrainMaterialParam::default(),
        // 30
        TerrainMaterialParam::default(),
        // 31
        TerrainMaterialParam {
            blend_sharpness: 0.147287,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 1.0,
            specularity_scale: 0.253378,
            specularity: 0.368132,
            specularity_base: 0.17033,
            falloff: 0.0,
        },
    ]
}
// ----------------------------------------------------------------------------
#[allow(clippy::excessive_precision)]
fn kaer_morhen_material_params() -> [TerrainMaterialParam; 30] {
    [
        // 01:
        TerrainMaterialParam {
            blend_sharpness: 0.3797470033,
            slope_base_dampening: 0.6204379797,
            specularity: 0.4032259881,
            specularity_base: 0.451613009,
            specularity_scale: 0.419355005,
            ..Default::default()
        },
        // 02:
        TerrainMaterialParam {
            blend_sharpness: 0.075000003,
            slope_base_dampening: 1.0,
            specularity: 0.6774190068,
            specularity_base: 0.548386991,
            ..Default::default()
        },
        // 03:
        TerrainMaterialParam {
            blend_sharpness: 0.1389999986,
            slope_base_dampening: 0.2599999905,
            specularity: 0.577113986,
            specularity_base: 0.5174130201,
            ..Default::default()
        },
        // 04:
        TerrainMaterialParam {
            specularity: 0.4776119888,
            specularity_base: 0.5519999862,
            ..Default::default()
        },
        // 05:
        TerrainMaterialParam {
            blend_sharpness: 0.139240995,
            slope_base_dampening: 1.0,
            specularity: 0.6290320158,
            specularity_base: 0.5479999781,
            ..Default::default()
        },
        // 06:
        TerrainMaterialParam {
            slope_base_dampening: 1.0,
            specularity: 0.5671640038,
            specularity_base: 0.562188983,
            ..Default::default()
        },
        // 07:
        TerrainMaterialParam {
            blend_sharpness: 0.2300000042,
            slope_base_dampening: 1.0,
            specularity: 0.6069650054,
            specularity_base: 0.5619999766,
            ..Default::default()
        },
        // 08:
        TerrainMaterialParam {
            blend_sharpness: 0.4487800002,
            slope_base_dampening: 0.2150000036,
            specularity: 0.75,
            specularity_base: 0.5174130201,
            ..Default::default()
        },
        // 09:
        TerrainMaterialParam {
            blend_sharpness: 0.2303919941,
            slope_base_dampening: 0.112999998,
            specularity: 0.4925369918,
            specularity_base: 0.52700001,
            ..Default::default()
        },
        // 10:
        TerrainMaterialParam {
            blend_sharpness: 0.2025319934,
            slope_base_dampening: 0.025316,
            slope_normal_dampening: 0.3759999871,
            specularity: 0.1935479939,
            specularity_base: 0.467741996,
            specularity_scale: 0.3870970011,
            ..Default::default()
        },
        // 11:
        TerrainMaterialParam {
            blend_sharpness: 0.177214995,
            slope_base_dampening: 0.4550000131,
            specularity: 0.3980099857,
            specularity_base: 0.4420000017,
            specularity_scale: 0.5024880171,
            ..Default::default()
        },
        // 12:
        TerrainMaterialParam {
            blend_sharpness: 0.1150000021,
            slope_base_dampening: 0.5690000057,
            specularity: 0.4726369977,
            specularity_base: 0.5469999909,
            ..Default::default()
        },
        // 13:
        TerrainMaterialParam {
            blend_sharpness: 0.150820002,
            slope_base_dampening: 0.1770000011,
            slope_normal_dampening: 0.376812011,
            specularity: 0.4676620066,
            specularity_base: 0.5009999871,
            ..Default::default()
        },
        // 14:
        TerrainMaterialParam {
            blend_sharpness: 0.0759489983,
            slope_base_dampening: 0.1449999958,
            specularity: 0.5370000005,
            specularity_base: 0.5,
            ..Default::default()
        },
        // 15:
        TerrainMaterialParam {
            blend_sharpness: 0.3670000136,
            slope_base_dampening: 0.3409999907,
            specularity: 0.5273630023,
            specularity_base: 0.5070000291,
            specularity_scale: 0.1442790031,
            ..Default::default()
        },
        // 16:
        TerrainMaterialParam {
            blend_sharpness: 0.0759489983,
            slope_base_dampening: 0.1889999956,
            specularity: 0.4079599977,
            specularity_base: 0.5170000196,
            ..Default::default()
        },
        // 17:
        TerrainMaterialParam {
            blend_sharpness: 0.151898995,
            slope_base_dampening: 0.2399999946,
            specularity: 0.4726369977,
            specularity_base: 0.5161290169,
            specularity_scale: 0.1612900048,
            ..Default::default()
        },
        // 18:
        TerrainMaterialParam {
            blend_sharpness: 0.0886079967,
            slope_base_dampening: 0.3030000031,
            specularity: 0.548386991,
            specularity_base: 0.5161290169,
            ..Default::default()
        },
        // 19:
        TerrainMaterialParam {
            blend_sharpness: 0.0632909983,
            slope_base_dampening: 1.0,
            specularity: 0.4354839921,
            specularity_base: 0.5070000291,
            ..Default::default()
        },
        // 20:
        TerrainMaterialParam {
            blend_sharpness: 0.3670890033,
            slope_base_dampening: 1.0,
            specularity: 0.2096769959,
            specularity_base: 0.419355005,
            specularity_scale: 0.7580649853,
            ..Default::default()
        },
        // 21:
        TerrainMaterialParam {
            blend_sharpness: 0.0886079967,
            slope_base_dampening: 0.189872995,
            specularity: 0.5323380232,
            specularity_base: 0.4970000088,
            specularity_scale: 0.3283579946,
            ..Default::default()
        },
        // 22:
        TerrainMaterialParam {
            blend_sharpness: 0.1139239967,
            slope_base_dampening: 0.2405059934,
            specularity: 0.288556993,
            specularity_base: 0.5960000157,
            ..Default::default()
        },
        // 23:
        TerrainMaterialParam {
            blend_sharpness: 0.0506329983,
            slope_base_dampening: 0.3037970066,
            specularity: 0.3930349946,
            specularity_base: 0.4925369918,
            ..Default::default()
        },
        // 24:
        TerrainMaterialParam {
            blend_sharpness: 0.1012659967,
            slope_base_dampening: 0.9240509868,
            specularity: 0.6119400263,
            specularity_base: 0.5170000196,
            ..Default::default()
        },
        // 25:
        TerrainMaterialParam {
            blend_sharpness: 0.1012659967,
            slope_base_dampening: 0.177214995,
            specularity: 0.5671640038,
            specularity_base: 1.0,
            ..Default::default()
        },
        // 26:
        TerrainMaterialParam {
            blend_sharpness: 0.0886079967,
            slope_base_dampening: 1.0,
            specularity: 0.3980099857,
            specularity_base: 0.4925369918,
            ..Default::default()
        },
        // 27:
        TerrainMaterialParam {
            blend_sharpness: 0.0886079967,
            slope_base_dampening: 0.4854010046,
            specularity: 0.8358209729,
            specularity_base: 0.5024880171,
            ..Default::default()
        },
        // 28:
        TerrainMaterialParam {
            blend_sharpness: 0.1265819967,
            slope_base_dampening: 1.0,
            specularity: 0.4776119888,
            specularity_base: 0.5124379992,
            ..Default::default()
        },
        // 29:
        TerrainMaterialParam {
            blend_sharpness: 0.189872995,
            slope_base_dampening: 0.987342,
            specularity: 0.5373129845,
            specularity_base: 0.5223879814,
            ..Default::default()
        },
        // 30:
        TerrainMaterialParam {
            specularity: 0.3134329915,
            specularity_base: 0.4726369977,
            specularity_scale: 0.5970150232,
            ..Default::default()
        },
    ]
}
// ----------------------------------------------------------------------------
#[allow(clippy::excessive_precision)]
fn wyzima_castle_material_params() -> [TerrainMaterialParam; 31] {
    [TerrainMaterialParam::default(); 31]
}
// ----------------------------------------------------------------------------
#[allow(clippy::excessive_precision)]
fn isle_of_mist_material_params() -> [TerrainMaterialParam; 31] {
    [
        // 01
        TerrainMaterialParam {
            blend_sharpness: 0.1700000018,
            slope_base_dampening: 0.224999994,
            slope_normal_dampening: 0.5728160143,
            specularity_scale: 0.1700000018,
            specularity: 0.224999994,
            specularity_base: 0.4779999852,
            ..Default::default()
        },
        // 02
        TerrainMaterialParam {
            specularity: 0.4029999971,
            specularity_base: 0.5640000105,
            ..Default::default()
        },
        // 03
        TerrainMaterialParam {
            specularity: 0.4169999957,
            specularity_base: 0.5099999905,
            ..Default::default()
        },
        // 04
        TerrainMaterialParam {
            specularity: 0.5159999728,
            specularity_base: 0.5640000105,
            ..Default::default()
        },
        // 05
        TerrainMaterialParam {
            specularity: 0.5490000248,
            specularity_base: 0.52700001,
            ..Default::default()
        },
        // 06
        TerrainMaterialParam {
            specularity: 0.2460000068,
            specularity_base: 0.5360000134,
            ..Default::default()
        },
        // 07
        TerrainMaterialParam {
            specularity_scale: 0.0160000008,
            specularity: 0.52700001,
            specularity_base: 0.5120000243,
            ..Default::default()
        },
        // 08
        TerrainMaterialParam {
            specularity_scale: 0.0780000016,
            specularity: 0.5070000291,
            specularity_base: 0.3899999857,
            ..Default::default()
        },
        // 09
        TerrainMaterialParam {
            specularity: 0.4169999957,
            specularity_base: 0.5490000248,
            ..Default::default()
        },
        // 10
        TerrainMaterialParam {
            blend_sharpness: 0.155340001,
            slope_base_dampening: 0.5048540235,
            slope_normal_dampening: 0.6601939797,
            specularity: 0.4169999957,
            specularity_base: 0.5490000248,
            falloff: 0.0194169991,
            ..Default::default()
        },
        // 11
        TerrainMaterialParam {
            specularity: 0.4169999957,
            specularity_base: 0.5490000248,
            ..Default::default()
        },
        // 12
        TerrainMaterialParam {
            specularity: 0.3619999886,
            specularity_base: 0.5149999857,
            ..Default::default()
        },
        // 13
        TerrainMaterialParam {
            specularity: 0.7699999809,
            specularity_base: 0.7480000257,
            ..Default::default()
        },
        // 14
        TerrainMaterialParam {
            specularity: 0.7699999809,
            specularity_base: 0.7089999914,
            ..Default::default()
        },
        // 15
        TerrainMaterialParam {
            specularity: 0.4830000103,
            specularity_base: 0.5479999781,
            ..Default::default()
        },
        // 16
        TerrainMaterialParam {
            specularity: 0.4830000103,
            specularity_base: 0.5479999781,
            ..Default::default()
        },
        // 17
        TerrainMaterialParam {
            specularity_scale: 0.773999989,
            specularity: 0.1609999985,
            specularity_base: 0.4189999998,
            ..Default::default()
        },
        // 18
        TerrainMaterialParam {
            specularity_scale: 0.9029999971,
            specularity: 0.3869999945,
            specularity_base: 0.5640000105,
            ..Default::default()
        },
        // 19
        TerrainMaterialParam {
            blend_sharpness: 0.194175005,
            slope_base_dampening: 0.5048540235,
            slope_normal_dampening: 0.4757280052,
            specularity: 0.2630000114,
            specularity_base: 0.5429999828,
            ..Default::default()
        },
        // 20
        TerrainMaterialParam {
            specularity: 0.4915249944,
            specularity_base: 0.4237290025,
            ..Default::default()
        },
        // 21
        TerrainMaterialParam {
            specularity_scale: 0.3389829993,
            specularity: 0.4915249944,
            specularity_base: 0.4915249944,
            ..Default::default()
        },
        // 22
        TerrainMaterialParam {
            specularity_scale: 0.0049999999,
            specularity: 0.4720000029,
            specularity_base: 0.5870000124,
            ..Default::default()
        },
        // 23
        TerrainMaterialParam {
            specularity_scale: 0.1864410043,
            specularity: 0.3898310065,
            specularity_base: 0.4915249944,
            ..Default::default()
        },
        // 24
        TerrainMaterialParam {
            specularity: 0.1199999973,
            specularity_base: 0.5870000124,
            ..Default::default()
        },
        // 25
        TerrainMaterialParam {
            specularity: 0.3790000081,
            specularity_base: 0.5490000248,
            ..Default::default()
        },
        // 26
        TerrainMaterialParam {
            blend_sharpness: 0.0873790011,
            slope_base_dampening: 0.8543689847,
            slope_normal_dampening: 0.9320390224,
            specularity_scale: 0.0338979997,
            specularity: 0.5084750056,
            specularity_base: 0.5254240036,
            falloff: 0.0970870033,
        },
        // 27
        TerrainMaterialParam {
            specularity_scale: 0.0329999998,
            specularity: 0.5080000162,
            specularity_base: 0.5249999762,
            ..Default::default()
        },
        // 28
        TerrainMaterialParam {
            specularity: 0.4067800045,
            specularity_base: 0.5423730016,
            ..Default::default()
        },
        // 29
        TerrainMaterialParam {
            specularity: 0.4889999926,
            specularity_base: 0.5379999876,
            ..Default::default()
        },
        // 30
        TerrainMaterialParam {
            specularity: 0.4889999926,
            specularity_base: 0.5490000248,
            ..Default::default()
        },
        // 31
        TerrainMaterialParam {
            specularity: 1.0,
            specularity_base: 1.0,
            ..Default::default()
        },
    ]
}
// ----------------------------------------------------------------------------
#[allow(clippy::excessive_precision)]
fn spiral_material_params() -> [TerrainMaterialParam; 26] {
    [
        // 1
        TerrainMaterialParam {
            blend_sharpness: 0.740741,
            slope_base_dampening: 0.348148,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.17,
            specularity: 0.233333,
            specularity_base: 0.478,
            falloff: 0.0,
        },
        // 2
        TerrainMaterialParam {
            blend_sharpness: 0.607407,
            slope_base_dampening: 0.421622,
            slope_normal_dampening: 0.52973,
            specularity_scale: 0.0,
            specularity: 0.52,
            specularity_base: 0.526667,
            falloff: 0.0,
        },
        // 3
        TerrainMaterialParam {
            blend_sharpness: 0.362963,
            slope_base_dampening: 0.311111,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.078125,
            specularity: 0.338542,
            specularity_base: 0.458333,
            falloff: 0.0,
        },
        // 4
        TerrainMaterialParam {
            specularity: 0.717,
            specularity_base: 0.528,
            falloff: 0.0,
            ..Default::default()
        },
        // 5
        TerrainMaterialParam {
            blend_sharpness: 0.007407,
            slope_base_dampening: 0.281481,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.369792,
            specularity_base: 0.5,
            falloff: 0.0,
        },
        // 6
        TerrainMaterialParam {
            blend_sharpness: 0.185185,
            slope_base_dampening: 0.274074,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.417,
            specularity_base: 0.51,
            falloff: 0.0,
        },
        // 7
        TerrainMaterialParam {
            specularity: 0.387,
            specularity_base: 0.532,
            falloff: 0.0,
            ..Default::default()
        },
        // 8
        TerrainMaterialParam {
            specularity: 0.379,
            specularity_base: 0.549,
            falloff: 0.0,
            ..Default::default()
        },
        // 9
        TerrainMaterialParam {
            blend_sharpness: 0.466667,
            slope_base_dampening: 0.066667,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.005,
            specularity: 0.472,
            specularity_base: 0.587,
            falloff: 0.0,
        },
        // 10
        TerrainMaterialParam {
            specularity: 0.12,
            specularity_base: 0.587,
            falloff: 0.0,
            ..Default::default()
        },
        // 11
        TerrainMaterialParam {
            specularity: 0.417,
            specularity_base: 0.549,
            falloff: 0.0,
            ..Default::default()
        },
        // 12
        TerrainMaterialParam {
            blend_sharpness: 0.274074,
            slope_base_dampening: 0.140741,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.506667,
            specularity_base: 0.52,
            falloff: 0.0,
        },
        // 13
        TerrainMaterialParam {
            blend_sharpness: 0.328283,
            slope_base_dampening: 0.363636,
            slope_normal_dampening: 1.0,
            specularity_scale: 0.472,
            specularity: 0.445,
            specularity_base: 0.478,
            falloff: 0.0,
        },
        // 14
        TerrainMaterialParam {
            blend_sharpness: 0.434343,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            specularity: 0.273333,
            specularity_base: 0.536458,
            falloff: 0.0,
        },
        // 15
        TerrainMaterialParam {
            blend_sharpness: 0.29798,
            slope_base_dampening: 0.2,
            slope_normal_dampening: 0.6,
            specularity_scale: 0.0,
            specularity: 0.635417,
            specularity_base: 0.546875,
            falloff: 0.0,
        },
        // 16
        TerrainMaterialParam {
            specularity: 0.380208,
            specularity_base: 0.526042,
            falloff: 0.0,
            ..Default::default()
        },
        // 17
        TerrainMaterialParam {
            blend_sharpness: 0.323308,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.274074,
            specularity_scale: 0.0,
            specularity: 0.77,
            specularity_base: 0.748092,
            falloff: 0.685484,
        },
        // 18
        TerrainMaterialParam {
            blend_sharpness: 0.47482,
            slope_base_dampening: 1.0,
            slope_normal_dampening: 0.402878,
            specularity_scale: 0.0,
            specularity: 0.77,
            specularity_base: 0.709,
            falloff: 0.0,
        },
        // 19
        TerrainMaterialParam {
            blend_sharpness: 0.0,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.036458,
            specularity: 0.510417,
            specularity_base: 0.593,
            falloff: 0.0,
        },
        // 20
        TerrainMaterialParam {
            blend_sharpness: 0.096296,
            slope_base_dampening: 0.340741,
            slope_normal_dampening: 0.333333,
            specularity_scale: 0.0,
            specularity: 0.739583,
            specularity_base: 0.505,
            falloff: 0.0,
        },
        // 21
        TerrainMaterialParam {
            specularity: 0.296875,
            specularity_base: 0.635417,
            falloff: 0.0,
            ..Default::default()
        },
        // 22
        TerrainMaterialParam {
            blend_sharpness: 0.0,
            slope_base_dampening: 0.446043,
            slope_normal_dampening: 0.273381,
            specularity_scale: 0.0,
            specularity: 0.77,
            specularity_base: 0.706,
            falloff: 0.0,
        },
        // 23
        TerrainMaterialParam {
            specularity: 0.52,
            specularity_base: 0.526667,
            falloff: 0.0,
            ..Default::default()
        },
        // 24
        TerrainMaterialParam {
            specularity: 0.326667,
            specularity_base: 0.566667,
            falloff: 0.0,
            ..Default::default()
        },
        // 25
        TerrainMaterialParam {
            specularity: 0.494792,
            specularity_base: 0.395833,
            falloff: 0.0,
            ..Default::default()
        },
        // 26
        TerrainMaterialParam {
            blend_sharpness: 0.785185,
            slope_base_dampening: 0.0,
            slope_normal_dampening: 0.0,
            specularity_scale: 0.0,
            ..Default::default()
        },
    ]
}
// ----------------------------------------------------------------------------
#[allow(clippy::excessive_precision)]
fn bob_material_params() -> [TerrainMaterialParam; 31] {
    [
        // 01
        TerrainMaterialParam {
            blend_sharpness: 0.200000003,
            slope_base_dampening: 0.3000000119,
            slope_normal_dampening: 1.0,
            specularity_scale: 0.5,
            specularity: 0.1000000015,
            specularity_base: 0.3555560112,
            ..Default::default()
        },
        // 02
        TerrainMaterialParam {
            blend_sharpness: 0.3000000119,
            slope_base_dampening: 0.4499999881,
            slope_normal_dampening: 1.0,
            specularity_scale: 0.5500000119,
            specularity: 0.1000000015,
            specularity_base: 0.3549999893,
            falloff: 0.0599999987,
        },
        // 03
        TerrainMaterialParam {
            blend_sharpness: 0.1000000015,
            slope_base_dampening: 0.1888889968,
            slope_normal_dampening: 0.6999999881,
            specularity_scale: 0.0444439985,
            specularity: 0.3777779937,
            specularity_base: 0.5,
            falloff: 0.1111110002,
        },
        // 04
        TerrainMaterialParam {
            blend_sharpness: 0.349999994,
            slope_base_dampening: 0.2704400122,
            slope_normal_dampening: 0.6999999881,
            specularity_scale: 0.4345239997,
            specularity: 0.2879999876,
            specularity_base: 0.2321429998,
            ..Default::default()
        },
        // 05
        TerrainMaterialParam {
            blend_sharpness: 0.2641510069,
            slope_base_dampening: 0.400000006,
            slope_normal_dampening: 0.6999999881,
            specularity_scale: 0.2800000012,
            specularity: 0.1770000011,
            specularity_base: 0.5519999862,
            ..Default::default()
        },
        // 06
        TerrainMaterialParam {
            blend_sharpness: 0.4339619875,
            slope_base_dampening: 0.3000000119,
            slope_normal_dampening: 0.8999999762,
            specularity_scale: 0.3962259889,
            specularity: 0.1000000015,
            specularity_base: 0.349999994,
            falloff: 0.0799999982,
        },
        // 07
        TerrainMaterialParam {
            blend_sharpness: 0.1000000015,
            slope_base_dampening: 0.2333330065,
            slope_normal_dampening: 1.0,
            specularity_scale: 0.5333330035,
            specularity: 0.2111109942,
            specularity_base: 0.2222220004,
            falloff: 0.0599999987,
        },
        // 08
        TerrainMaterialParam {
            blend_sharpness: 0.3666670024,
            slope_base_dampening: 0.5031449795,
            slope_normal_dampening: 0.8000000119,
            specularity_scale: 0.2222220004,
            specularity: 0.0888890028,
            specularity_base: 0.3666670024,
            falloff: 0.0599999987,
        },
        // 09
        TerrainMaterialParam {
            blend_sharpness: 0.1800000072,
            slope_base_dampening: 0.3222219944,
            slope_normal_dampening: 1.0,
            specularity_scale: 0.4659999907,
            specularity: 0.2109999955,
            specularity_base: 0.2109999955,
            falloff: 0.0599999987,
        },
        // 10
        TerrainMaterialParam {
            slope_normal_dampening: 1.0,
            specularity_scale: 0.5,
            specularity: 0.0888890028,
            falloff: 0.1770000011,
            ..Default::default()
        },
        // 11
        TerrainMaterialParam {
            blend_sharpness: 0.1607140005,
            slope_base_dampening: 0.7200000286,
            slope_normal_dampening: 0.5,
            specularity: 0.200000003,
            specularity_base: 1.0,
            falloff: 0.1000000015,
            ..Default::default()
        },
        // 12
        TerrainMaterialParam {
            blend_sharpness: 0.3869050145,
            slope_base_dampening: 0.1199999973,
            slope_normal_dampening: 0.3199999928,
            specularity: 0.2399999946,
            specularity_base: 1.0,
            falloff: 0.1000000015,
            ..Default::default()
        },
        // 13
        TerrainMaterialParam {
            blend_sharpness: 0.3860000074,
            slope_normal_dampening: 0.6800000072,
            specularity_scale: 1.0,
            specularity: 0.1599999964,
            falloff: 0.4499999881,
            ..Default::default()
        },
        // 14
        TerrainMaterialParam {
            blend_sharpness: 0.3000000119,
            slope_base_dampening: 0.48427701,
            slope_normal_dampening: 1.0,
            specularity_scale: 0.6157230139,
            specularity: 0.1320749968,
            specularity_base: 0.3396230042,
            ..Default::default()
        },
        // 15
        TerrainMaterialParam {
            blend_sharpness: 0.3000000119,
            slope_base_dampening: 0.0799999982,
            specularity_scale: 1.0,
            specularity: 0.1333329976,
            specularity_base: 0.1666669995,
            ..Default::default()
        },
        // 16
        TerrainMaterialParam {
            blend_sharpness: 0.1888889968,
            slope_base_dampening: 0.1555559933,
            slope_normal_dampening: 0.8666669726,
            specularity_scale: 0.0314470008,
            specularity: 0.3199999928,
            specularity_base: 0.4905659854,
            falloff: 0.1199999973,
        },
        // 17
        TerrainMaterialParam {
            blend_sharpness: 0.3000000119,
            slope_base_dampening: 0.2327039987,
            slope_normal_dampening: 0.6352199912,
            specularity_scale: 0.4968549907,
            specularity: 0.1132080033,
            specularity_base: 0.51572299,
            falloff: 0.1006290019,
        },
        // 18
        TerrainMaterialParam {
            blend_sharpness: 0.200000003,
            slope_base_dampening: 0.1444440037,
            slope_normal_dampening: 0.8555560112,
            specularity_scale: 0.1011900008,
            specularity: 0.1607140005,
            specularity_base: 0.6000000238,
            falloff: 0.066666998,
        },
        // 19
        TerrainMaterialParam {
            slope_base_dampening: 0.6000000238,
            slope_normal_dampening: 0.7222219706,
            specularity_scale: 0.8999999762,
            specularity: 0.0888890028,
            falloff: 0.1222219989,
            ..Default::default()
        },
        // 20
        TerrainMaterialParam {
            blend_sharpness: 0.2704400122,
            slope_base_dampening: 0.276730001,
            slope_normal_dampening: 0.8993710279,
            specularity_scale: 0.2800000012,
            specularity: 0.1777780056,
            specularity_base: 0.5222219825,
            ..Default::default()
        },
        // 21
        TerrainMaterialParam {
            blend_sharpness: 0.200000003,
            slope_base_dampening: 0.6499999762,
            slope_normal_dampening: 0.4499999881,
            specularity_scale: 0.5408809781,
            specularity: 0.2264149934,
            specularity_base: 0.220126003,
            falloff: 0.1800000072,
        },
        // 22
        TerrainMaterialParam {
            blend_sharpness: 0.1449999958,
            slope_base_dampening: 0.6499999762,
            slope_normal_dampening: 0.6999999881,
            specularity_scale: 0.4025160074,
            specularity: 0.2515720129,
            specularity_base: 0.2578620017,
            falloff: 0.1350000054,
        },
        // 23
        TerrainMaterialParam {
            blend_sharpness: 0.1199999973,
            slope_base_dampening: 0.2738099992,
            slope_normal_dampening: 1.0,
            specularity_scale: 0.464150995,
            specularity: 0.1949689984,
            specularity_base: 0.48427701,
            falloff: 0.1080000028,
        },
        // 24
        TerrainMaterialParam {
            blend_sharpness: 0.1400000006,
            slope_base_dampening: 0.3600000143,
            slope_normal_dampening: 1.0,
            specularity_scale: 0.4339619875,
            specularity: 0.1509429961,
            specularity_base: 0.7106919885,
            falloff: 0.1700000018,
        },
        // 25
        TerrainMaterialParam {
            slope_base_dampening: 0.1199999973,
            slope_normal_dampening: 0.2199990004,
            specularity: 0.1199999973,
            specularity_base: 1.0,
            falloff: 0.200000003,
            ..Default::default()
        },
        // 26
        TerrainMaterialParam {
            slope_base_dampening: 0.2800000012,
            slope_normal_dampening: 0.1964289993,
            specularity: 0.1800000072,
            specularity_base: 1.0,
            falloff: 0.200000003,
            ..Default::default()
        },
        // 27
        TerrainMaterialParam {
            blend_sharpness: 0.2399999946,
            slope_base_dampening: 0.0799999982,
            slope_normal_dampening: 0.2399999946,
            specularity_scale: 1.0,
            specularity: 0.1785710007,
            specularity_base: 0.1011900008,
            falloff: 0.066666998,
        },
        // 28
        TerrainMaterialParam {
            slope_base_dampening: 0.3214290142,
            slope_normal_dampening: 0.1488099992,
            specularity: 0.1199999973,
            specularity_base: 1.0,
            falloff: 0.200000003,
            ..Default::default()
        },
        // 29
        TerrainMaterialParam {
            blend_sharpness: 0.3000000119,
            slope_normal_dampening: 0.5,
            specularity_scale: 0.6444439888,
            specularity: 0.2777779996,
            specularity_base: 0.1777780056,
            falloff: 0.1000000015,
            ..Default::default()
        },
        // 30
        TerrainMaterialParam {
            blend_sharpness: 0.0500000007,
            slope_base_dampening: 0.6499999762,
            slope_normal_dampening: 0.8805029988,
            specularity_scale: 0.7106919885,
            specularity: 0.1446540058,
            specularity_base: 0.0628930032,
            falloff: 0.1964289993,
        },
        // 31
        TerrainMaterialParam {
            slope_base_dampening: 0.6826350093,
            slope_normal_dampening: 0.5389220119,
            specularity_scale: 1.0,
            specularity_base: 0.0838320032,
            falloff: 0.3532930017,
            ..Default::default()
        },
    ]
}
// ----------------------------------------------------------------------------

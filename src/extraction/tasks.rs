// ----------------------------------------------------------------------------
use bevy::tasks::Task;

use super::{
    AssembleMapTask, BundleExtractionTask, DataType, ExtractArrayLayerTask, ExtractionTask,
    StoreMapTask, StoreTerrainTextureTask, TextureExtractionTask,
};
// ----------------------------------------------------------------------------
// async tasks + results
// ----------------------------------------------------------------------------
pub(super) enum AsyncTaskResult<T, E> {
    Ok(T),
    MultipleOk(Vec<T>),
    Err(E),
}
// ----------------------------------------------------------------------------
pub(super) trait AsyncTask<R> {
    fn future_mut(&mut self) -> &mut Task<AsyncTaskResult<R, String>>;
}
// ----------------------------------------------------------------------------
impl AsyncTask<DataType> for BundleExtractionTask {
    fn future_mut(&mut self) -> &mut Task<AsyncTaskResult<DataType, String>> {
        &mut self.0
    }
}
// ----------------------------------------------------------------------------
impl AsyncTask<DataType> for TextureExtractionTask {
    fn future_mut(&mut self) -> &mut Task<AsyncTaskResult<DataType, String>> {
        &mut self.0
    }
}
// ----------------------------------------------------------------------------
impl AsyncTask<DataType> for AssembleMapTask {
    fn future_mut(&mut self) -> &mut Task<AsyncTaskResult<DataType, String>> {
        &mut self.0
    }
}
// ----------------------------------------------------------------------------
impl AsyncTask<()> for StoreMapTask {
    fn future_mut(&mut self) -> &mut Task<AsyncTaskResult<(), String>> {
        &mut self.0
    }
}
// ----------------------------------------------------------------------------
impl AsyncTask<DataType> for ExtractArrayLayerTask {
    fn future_mut(&mut self) -> &mut Task<AsyncTaskResult<DataType, String>> {
        &mut self.0
    }
}
// ----------------------------------------------------------------------------
impl AsyncTask<()> for StoreTerrainTextureTask {
    fn future_mut(&mut self) -> &mut Task<AsyncTaskResult<(), String>> {
        &mut self.0
    }
}
// ----------------------------------------------------------------------------
// captions for progress msg
// ----------------------------------------------------------------------------
impl ExtractionTask {
    // ------------------------------------------------------------------------
    pub fn caption(&self) -> Option<String> {
        match self {
            ExtractionTask::ExtractBundleFiles(_) => Some("extracting files".to_string()),
            ExtractionTask::ExtractTextureFiles(_) => Some("extracting textures".to_string()),
            ExtractionTask::TaskPending => None,
            ExtractionTask::AssembleMap(_, maptype) => Some(format!("merging {} tiles", maptype)),
            ExtractionTask::SplitMap(_) => Some("creating submaps".to_string()),
            ExtractionTask::StoreMaps => Some("storing map".to_string()),
            ExtractionTask::ExtractTextureArrayLayer(_) => {
                Some("extracting texturearray layers".to_string())
            }
            ExtractionTask::StoreTerrainTextures(t) => {
                Some(format!("storing {} terrain textures", t))
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// AsyncTaskResult converter
// ----------------------------------------------------------------------------
impl<R, E> AsyncTaskResult<R, E> {
    pub fn into_vec(self) -> Vec<Result<R, E>> {
        match self {
            AsyncTaskResult::Ok(v) => vec![Ok(v)],
            AsyncTaskResult::Err(m) => vec![Err(m)],
            AsyncTaskResult::MultipleOk(mut vec) => vec.drain(..).map(Ok).collect(),
        }
    }
}
// ----------------------------------------------------------------------------
impl<R, E> From<Result<R, E>> for AsyncTaskResult<R, E> {
    fn from(r: Result<R, E>) -> Self {
        match r {
            Ok(v) => Self::Ok(v),
            Err(e) => Self::Err(e),
        }
    }
}
// ----------------------------------------------------------------------------
impl<R, E> From<Result<Vec<R>, E>> for AsyncTaskResult<R, E> {
    fn from(r: Result<Vec<R>, E>) -> Self {
        match r {
            Ok(v) => Self::MultipleOk(v),
            Err(e) => Self::Err(e),
        }
    }
}
// ----------------------------------------------------------------------------

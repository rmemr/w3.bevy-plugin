// ----------------------------------------------------------------------------
#[derive(Debug)]
pub struct ProgressTrackingState {
    pub request: ExtractionRequest,
    pub task_current: usize,
    pub task_max: usize,
    pub current: usize,
    pub max: usize,
    msg: Option<String>,
}
// ----------------------------------------------------------------------------
#[derive(Hash, PartialEq, Eq, Copy, Clone, Debug)]
pub struct TaskId(pub &'static str);
// ----------------------------------------------------------------------------
pub struct TaskProgress {
    request: ExtractionRequest,
    request_current: usize,
    request_max: usize,

    _task: TaskId,
    task_current: usize,
    task_max: usize,
    task_caption: Option<String>,
}
// ----------------------------------------------------------------------------
pub struct RequestProgress {
    request: ExtractionRequest,
    tasks: Vec<(TaskId, usize)>,
    max: usize,
}
// ----------------------------------------------------------------------------
use super::ExtractionRequest;
// ----------------------------------------------------------------------------
impl RequestProgress {
    // ------------------------------------------------------------------------
    pub fn new(tracked_request: ExtractionRequest) -> Self {
        Self {
            request: tracked_request,
            tasks: Vec::default(),
            max: 0,
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_task(mut self, taskid: TaskId, max: usize) -> Self {
        self.tasks.push((taskid, max));
        self.max += max;
        self
    }
    // ------------------------------------------------------------------------
    pub fn start_state(&self) -> ProgressTrackingState {
        ProgressTrackingState {
            request: self.request,
            task_current: 0,
            task_max: self.max,
            current: 0,
            max: self.max,
            msg: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn into_tasks(mut self) -> Vec<TaskProgress> {
        let mut start = 0;
        self.tasks
            .drain(..)
            .map(|(id, max)| {
                start += max;
                TaskProgress {
                    request: self.request,
                    request_current: start - max,
                    request_max: self.max,
                    _task: id,
                    task_current: 0,
                    task_max: max,
                    task_caption: None,
                }
            })
            .collect()
    }
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TaskProgress {
    // ------------------------------------------------------------------------
    pub fn set_task_caption(mut self, caption: Option<String>) -> Self {
        self.task_caption = caption;
        self
    }
    // ------------------------------------------------------------------------
    pub fn update_processed(&mut self, processed: usize) -> ProgressTrackingState {
        self.task_current = self.task_max.min(self.task_current + processed);
        self.state()
    }
    // ------------------------------------------------------------------------
    pub fn state(&self) -> ProgressTrackingState {
        ProgressTrackingState {
            request: self.request,
            task_current: self.task_current,
            task_max: self.task_max,
            current: self.request_current + self.task_current,
            max: self.request_max,
            msg: self.task_caption.clone(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ProgressTrackingState {
    // ------------------------------------------------------------------------
    pub fn is_finished(&self) -> bool {
        self.current >= self.max
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn task_progress(&self) -> f32 {
        self.task_current as f32 / (self.task_max as f32).max(f32::MIN_POSITIVE)
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    pub fn request_progress(&self) -> f32 {
        self.current as f32 / (self.max as f32).max(f32::MIN_POSITIVE)
    }
    // ------------------------------------------------------------------------
    pub fn task_progress_msg(&self) -> Option<String> {
        self.msg.as_ref().map(|msg| {
            match self.task_max {
                1 => format!("{}: {}...", self.request, msg),
                2..=50 => format!(
                    "{}: {}...{}/{}",
                    self.request,
                    msg,
                    // don't start with zero for explicit tasks
                    self.task_max.min(self.task_current + 1),
                    self.task_max
                ),
                _ => format!(
                    "{}: {}...{}%",
                    self.request,
                    msg,
                    (self.task_progress() * 100.0).trunc()
                ),
            }
        })
    }
    // ------------------------------------------------------------------------
    pub fn request_progress_msg(&self) -> Option<String> {
        self.msg
            .as_ref()
            .map(|msg| format!("{}...{}%", msg, (self.request_progress() * 100.0).trunc()))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Default for TaskProgress {
    fn default() -> Self {
        Self {
            request: ExtractionRequest::None,
            request_current: 0,
            request_max: 0,
            _task: TaskId(""),
            task_current: 0,
            task_max: 0,
            task_caption: None,
        }
    }
}
// ----------------------------------------------------------------------------

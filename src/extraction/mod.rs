// ----------------------------------------------------------------------------
use bevy::ecs::schedule::StateData;
use bevy::prelude::*;
use bevy::tasks::{AsyncComputeTaskPool, Task};
use futures_lite::future;

use crate::{Witcher3Config, Witcher3HubId};

use self::progress::TaskProgress;
use self::reader::{BundleRegistry, TextureCacheRegistry};
use self::tasks::{AsyncTask, AsyncTaskResult};
// ----------------------------------------------------------------------------
pub struct Witcher3ExtractPlugin;
// ----------------------------------------------------------------------------
pub use self::progress::ProgressTrackingState;
// ----------------------------------------------------------------------------
#[derive(Copy, Clone, Debug)]
pub enum ExtractionRequest {
    None,
    TerrainData(Witcher3HubId),
    TerrainTextures(Witcher3HubId),
}
// ----------------------------------------------------------------------------
#[derive(Debug)]
pub enum ExtractionProgress {
    ProgressTrackingStart(String, Vec<ProgressTrackingState>),
    ProgressTrackingCancel,
    ProgressTrackingUpdate(ProgressTrackingState),
    ExtractionFinished,
}
// ----------------------------------------------------------------------------
impl Witcher3ExtractPlugin {
    // ------------------------------------------------------------------------
    pub fn extract_data<T: StateData>(state: T) -> SystemSet {
        SystemSet::on_enter(state)
            .with_system(prepare_extraction_tasks.chain(handle_extraction_errors))
    }
    // ------------------------------------------------------------------------
    fn task_processing<T: StateData>(state: T) -> SystemSet {
        SystemSet::on_update(state)
            .with_system(start_next_extraction_task.chain(handle_extraction_errors))
            .with_system(handle_active_task.chain(handle_extraction_errors))
    }
    // ------------------------------------------------------------------------
    fn extraction_tasks<T: StateData>(state: T) -> SystemSet {
        SystemSet::on_update(state)
            .with_system(handle_tasks::<BundleExtractionTask>.chain(handle_extraction_errors))
            .with_system(handle_tasks::<TextureExtractionTask>.chain(handle_extraction_errors))
            .with_system(handle_tasks::<AssembleMapTask>.chain(handle_extraction_errors))
            .with_system(handle_tasks::<ExtractArrayLayerTask>.chain(handle_extraction_errors))
            .with_system(handle_leaf_tasks::<StoreMapTask>.chain(handle_extraction_errors))
            .with_system(
                handle_leaf_tasks::<StoreTerrainTextureTask>.chain(handle_extraction_errors),
            )
    }
    // ------------------------------------------------------------------------
    fn extraction_finished<T: StateData>(state: T) -> SystemSet {
        SystemSet::on_enter(state).with_system(cleanup_after_finish)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
enum PluginState {
    Deactivated,
    ExtractData,
    ExtractionFinished,
}
// ----------------------------------------------------------------------------
mod progress;
mod reader;
mod tasks;
mod terrain;
mod textures;
// ----------------------------------------------------------------------------
impl Plugin for Witcher3ExtractPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<ExtractionProgress>()
            .init_resource::<Vec<ExtractionRequest>>()
            .init_resource::<BundleRegistry>()
            .init_resource::<TextureCacheRegistry>()
            .init_resource::<ExtractionTasks>()
            .init_resource::<ActiveTask>()
            .init_resource::<DataQueue>()
            .init_resource::<TaskProgress>()
            .add_state(PluginState::Deactivated);

        use PluginState::*;
        app.add_system_set(Self::task_processing(ExtractData).label("task_processing"))
            .add_system_set(Self::extraction_tasks(ExtractData).after("task_processing"))
            .add_system_set(Self::extraction_finished(ExtractionFinished));
    }
}
// ----------------------------------------------------------------------------
// tasks
// ----------------------------------------------------------------------------
enum ExtractionTask {
    TaskPending,
    ExtractBundleFiles(Box<dyn ExtractBundleFilesRequest>),
    ExtractTextureFiles(Box<dyn ExtractTexturesRequest>),
    AssembleMap(Witcher3HubId, terrain::ExtractionType),
    SplitMap(Vec<(Witcher3HubId, terrain::MapSection)>),
    StoreMaps,
    ExtractTextureArrayLayer(Witcher3HubId),
    StoreTerrainTextures(textures::TextureType),
}
// ----------------------------------------------------------------------------
trait ExtractBundleFilesRequest: Sync + Send {
    fn generate_tasks(&self, bundles: Res<BundleRegistry>) -> Vec<BundleExtractionTask>;
}
// ----------------------------------------------------------------------------
trait ExtractTexturesRequest: Sync + Send {
    fn generate_tasks(&self, caches: Res<TextureCacheRegistry>) -> Vec<TextureExtractionTask>;
}
// ----------------------------------------------------------------------------
#[derive(Component)]
struct BundleExtractionTask(Task<AsyncTaskResult<DataType, String>>);
#[derive(Component)]
struct TextureExtractionTask(Task<AsyncTaskResult<DataType, String>>);
#[derive(Component)]
struct AssembleMapTask(Task<AsyncTaskResult<DataType, String>>);
#[derive(Component)]
struct StoreMapTask(Task<AsyncTaskResult<(), String>>);
#[derive(Component)]
struct ExtractArrayLayerTask(Task<AsyncTaskResult<DataType, String>>);
#[derive(Component)]
struct StoreTerrainTextureTask(Task<AsyncTaskResult<(), String>>);
// ----------------------------------------------------------------------------
// system resources
// ----------------------------------------------------------------------------
enum DataType {
    TerrainTileData(terrain::TileData),
    TerrainData(Witcher3HubId, terrain::TerrainData),
    TextureData(textures::TextureData),
    TerrainTextureDds(Witcher3HubId, u8, ddsfile::Dds),
}
// ----------------------------------------------------------------------------
#[derive(Default, DerefMut, Deref)]
struct DataQueue(Vec<DataType>);
// ----------------------------------------------------------------------------
#[derive(Default, DerefMut, Deref)]
struct ExtractionTasks(Vec<(ExtractionTask, TaskProgress)>);
// ----------------------------------------------------------------------------
#[derive(Default, DerefMut, Deref)]
struct ActiveTask(Option<ExtractionTask>);
// ----------------------------------------------------------------------------
// systems
// ----------------------------------------------------------------------------
fn prepare_extraction_tasks(
    conf: Res<Witcher3Config>,
    mut requests: ResMut<Vec<ExtractionRequest>>,
    mut bundles: ResMut<BundleRegistry>,
    mut texturecaches: ResMut<TextureCacheRegistry>,
    mut tasks: ResMut<ExtractionTasks>,
    mut state: ResMut<State<PluginState>>,
    mut event: EventWriter<ExtractionProgress>,
) -> Result<(), String> {
    let mut tracking_events = Vec::default();
    tasks.clear();

    let mut extract_terraindata = false;
    let mut extract_terraintextures = false;

    for request in requests.drain(..) {
        let (tracking_state, mut hub_tasks) = match request {
            ExtractionRequest::TerrainData(hubid) => {
                extract_terraindata = true;
                hubid.terrain_map_extraction_tasks()
            }
            ExtractionRequest::TerrainTextures(hubid) => {
                extract_terraintextures = true;
                hubid.terrain_textures_extraction_tasks()
            }
            ExtractionRequest::None => continue,
        };
        tracking_events.push(tracking_state);
        tasks.append(&mut hub_tasks);
    }

    if extract_terraindata {
        // scan bundles for terrain data
        bundles.init(&conf.game_dir, Some(".w2ter"))?;
    }
    if extract_terraintextures {
        // scan caches for terrain textures
        texturecaches.init(&conf.game_dir, Some(".texarray"))?;
    }

    tasks.0.reverse();
    event.send(ExtractionProgress::ProgressTrackingStart(
        "Extracting data".into(),
        tracking_events,
    ));

    state.overwrite_set(PluginState::ExtractData).unwrap();

    Ok(())
}
// ----------------------------------------------------------------------------
fn start_next_extraction_task(
    mut tasks: ResMut<ExtractionTasks>,
    mut active_task: ResMut<ActiveTask>,
    mut active_task_tracking: ResMut<TaskProgress>,
    mut state: ResMut<State<PluginState>>,
    mut event: EventWriter<ExtractionProgress>,
) -> Result<(), String> {
    if active_task.is_none() {
        if let Some((task, tracking)) = tasks.pop() {
            let progress = tracking.set_task_caption(task.caption());
            event.send(ExtractionProgress::ProgressTrackingUpdate(progress.state()));

            *active_task = ActiveTask(Some(task));
            *active_task_tracking = progress;
        } else {
            state
                .overwrite_set(PluginState::ExtractionFinished)
                .unwrap();
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn handle_tasks<T: Component + AsyncTask<DataType>>(
    mut commands: Commands,
    mut tasks: Query<(Entity, &mut T)>,
    mut active_task: ResMut<ActiveTask>,
    mut progress: ResMut<TaskProgress>,
    mut event: EventWriter<ExtractionProgress>,
    mut result: Local<Vec<Result<DataType, String>>>,
) -> Result<(), String> {
    use ExtractionProgress::ProgressTrackingUpdate;

    if !tasks.is_empty() {
        let mut remaining = 0;
        let mut processed = 0;
        for (entity, mut task) in &mut tasks {
            if let Some(store_result) = future::block_on(future::poll_once(task.future_mut())) {
                processed += 1;
                result.append(&mut store_result.into_vec());

                // remove task component from entity
                commands.entity(entity).remove::<T>();
            } else {
                remaining += 1;
            }
        }
        if remaining == 0 {
            // result as input to next step
            commands.insert_resource(DataQueue(result.drain(..).collect::<Result<Vec<_>, _>>()?));
            active_task.take();
        }

        event.send(ProgressTrackingUpdate(progress.update_processed(processed)));
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn handle_leaf_tasks<T: Component + AsyncTask<()>>(
    mut commands: Commands,
    mut tasks: Query<(Entity, &mut T)>,
    mut active_task: ResMut<ActiveTask>,
    mut progress: ResMut<TaskProgress>,
    mut event: EventWriter<ExtractionProgress>,
    mut result: Local<Vec<Result<(), String>>>,
) -> Result<(), String> {
    use ExtractionProgress::ProgressTrackingUpdate;

    if !tasks.is_empty() {
        let mut remaining = 0;
        let mut processed = 0;
        for (entity, mut task) in &mut tasks {
            if let Some(store_result) = future::block_on(future::poll_once(task.future_mut())) {
                processed += 1;
                result.append(&mut store_result.into_vec());

                // remove task component from entity
                commands.entity(entity).remove::<T>();
            } else {
                remaining += 1;
            }
        }
        if remaining == 0 {
            // no data for further processing - but check result
            result.drain(..).collect::<Result<Vec<_>, _>>()?;

            commands.insert_resource(DataQueue(Vec::default()));
            active_task.take();
        }

        event.send(ProgressTrackingUpdate(progress.update_processed(processed)));
    }
    Ok(())
}
// ----------------------------------------------------------------------------
#[allow(clippy::too_many_arguments)]
fn handle_active_task(
    mut commands: Commands,
    conf: Res<Witcher3Config>,
    bundles: Res<BundleRegistry>,
    textures: Res<TextureCacheRegistry>,
    mut data_queue: ResMut<DataQueue>,
    mut active_task: ResMut<ActiveTask>,
    mut progress: ResMut<TaskProgress>,
    mut event: EventWriter<ExtractionProgress>,
) -> Result<(), String> {
    if let Some(task) = active_task.take() {
        use ExtractionTask::*;
        let thread_pool = AsyncComputeTaskPool::get();

        *active_task = match task {
            TaskPending => {
                // just wait for running task to complete. it will clear ActiveTask itself
                ActiveTask(Some(TaskPending))
            }
            ExtractBundleFiles(request) => {
                for task in request.generate_tasks(bundles) {
                    commands.spawn().insert(task);
                }
                // this task may run multiple frames
                ActiveTask(Some(TaskPending))
            }
            ExtractTextureFiles(request) => {
                for task in request.generate_tasks(textures) {
                    commands.spawn().insert(task);
                }
                // this task may run multiple frames
                ActiveTask(Some(TaskPending))
            }
            AssembleMap(hubid, maptype) => {
                for task in terrain::assemble_map(thread_pool, hubid, maptype, &mut data_queue) {
                    commands.spawn().insert(task);
                }

                ActiveTask(Some(TaskPending))
            }
            SplitMap(sections) => {
                //TODO change to one-shot system?
                let result = terrain::split_map_into_sections(&sections, &mut data_queue)?;

                // complete result as input to next step
                commands.insert_resource(DataQueue(result));
                event.send(ExtractionProgress::ProgressTrackingUpdate(
                    progress.update_processed(1),
                ));

                ActiveTask(None)
            }
            StoreMaps => {
                for task in terrain::generate_store_maps_tasks(
                    thread_pool,
                    &conf.terrain.terrain_dir,
                    &mut data_queue,
                ) {
                    commands.spawn().insert(task);
                }
                ActiveTask(Some(TaskPending))
            }
            ExtractTextureArrayLayer(hubid) => {
                for task in terrain::extract_texturearray_layer(thread_pool, hubid, &mut data_queue)
                {
                    commands.spawn().insert(task);
                }

                ActiveTask(Some(TaskPending))
            }
            StoreTerrainTextures(texture_type) => {
                for task in terrain::store_terraintextures(
                    thread_pool,
                    &conf.terrain.texture_dir,
                    texture_type,
                    &mut data_queue,
                ) {
                    commands.spawn().insert(task);
                }
                ActiveTask(Some(TaskPending))
            }
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn handle_extraction_errors(
    In(result): In<Result<(), String>>,
    mut state: ResMut<State<PluginState>>,
    mut event: EventWriter<ExtractionProgress>,
) {
    match result {
        Ok(_) => {}
        Err(msg) => {
            error!("failed to extract data. {}", msg);
            event.send(ExtractionProgress::ProgressTrackingCancel);
            state
                .overwrite_set(PluginState::ExtractionFinished)
                .unwrap();
        }
    }
}
// ----------------------------------------------------------------------------
fn cleanup_after_finish(
    mut state: ResMut<State<PluginState>>,
    mut tasks: ResMut<ExtractionTasks>,
    mut active_task: ResMut<ActiveTask>,
    mut data_queue: ResMut<DataQueue>,
    mut event: EventWriter<ExtractionProgress>,
) {
    // cleanup resources (all resources since this system will also be called after error handler)
    tasks.clear();
    active_task.take();
    data_queue.clear();

    state.overwrite_set(PluginState::Deactivated).unwrap();
    event.send(ExtractionProgress::ExtractionFinished);
}
// ----------------------------------------------------------------------------
// captions for progress msg
// ----------------------------------------------------------------------------
impl std::fmt::Display for ExtractionRequest {
    // ------------------------------------------------------------------------
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ExtractionRequest::None => write!(f, ""),
            ExtractionRequest::TerrainData(hubid) | ExtractionRequest::TerrainTextures(hubid) => {
                write!(f, "{}", hubid.short_caption())
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

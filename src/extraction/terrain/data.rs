// ----------------------------------------------------------------------------
use std::ops::{Shl, Shr};
use std::path::Path;
use std::sync::Arc;

use bevy::prelude::*;
use bevy::tasks::AsyncComputeTaskPool;
use png::{BitDepth, ColorType, Compression};

use crate::utils;
use crate::Witcher3HubId;

use super::reader::{BundleRegistry, ItemDataReader};
use super::{
    AssembleMapTask, BundleExtractionTask, DataType, ExtractBundleFilesRequest, ExtractTerrainData,
    StoreMapTask,
};
// ----------------------------------------------------------------------------
#[derive(Clone, Copy, Debug)]
pub enum ExtractionType {
    Heightmap,
    TextureMaps,
    TintMap,
}
// ----------------------------------------------------------------------------
pub struct TileData {
    x: usize,
    y: usize,
    data: Vec<u8>,
}
// ----------------------------------------------------------------------------
pub enum TerrainData {
    Heightmap(Vec<u8>),
    BackgroundTexturing(Vec<u8>),
    OverlayTexturing(Vec<u8>),
    BlendControl(Vec<u8>),
    Tintmap(Vec<u8>),
}
// ----------------------------------------------------------------------------
#[derive(Clone, Copy, Debug)]
pub struct MapSection {
    pos: (u32, u32),
    size: u32,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
struct HubBundleDataParams {
    directory: &'static str,
    res: usize,
    x_tiles: usize,
    y_tiles: usize,
    colormap_start_mip: u32,
}
// ----------------------------------------------------------------------------
impl ExtractBundleFilesRequest for ExtractTerrainData {
    // ------------------------------------------------------------------------
    fn generate_tasks(&self, bundles: Res<BundleRegistry>) -> Vec<BundleExtractionTask> {
        use ExtractionType::*;

        let Self(hubid, maptype) = self;
        let params = hubid.bundle_data_params();

        let buffer = match maptype {
            Heightmap => params.heightmap_buffer(),
            TextureMaps => params.texturemap_buffer(),
            TintMap => params.tintmap_buffer(),
        };
        let buffer_filter = format!(".{}.buffer", buffer);

        let bundle = Arc::new(
            bundles
                .filter(|name| name.contains(params.directory) && name.contains(&buffer_filter))
                // strip path and keep only file name now that it's filtered by hub
                .shorten_names(),
        );
        let thread_pool = AsyncComputeTaskPool::get();

        let mut tasks = Vec::with_capacity(params.x_tiles * params.y_tiles);
        for x in 0..params.x_tiles {
            for y in 0..params.y_tiles {
                let tile = format!(
                    "tile_{}_x_{}_res{}.w2ter.{}.buffer",
                    y, x, params.res, buffer
                );
                let bundle = bundle.clone();

                let task = thread_pool.spawn(async move {
                    bundle
                        .read_data(&tile)
                        .map(|data| DataType::TerrainTileData(TileData { x, y, data }))
                        .into()
                });
                tasks.push(BundleExtractionTask(task));
            }
        }
        tasks
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
pub(in crate::extraction) fn assemble_map(
    thread_pool: &AsyncComputeTaskPool,
    hubid: Witcher3HubId,
    maptype: ExtractionType,
    input_data: &mut Vec<DataType>,
) -> Vec<AssembleMapTask> {
    use DataType::*;
    use ExtractionType::*;

    let tiles = input_data
        .drain(..)
        .map(|d| match d {
            TerrainTileData(data) => data,
            _ => unreachable!("wrong data type for AssembleMap"),
        })
        .collect::<Vec<_>>();

    let task = match maptype {
        Heightmap => thread_pool.spawn(async move {
            assemble_heightmap(&hubid.bundle_data_params(), tiles)
                .map(|data| TerrainData(hubid, data))
                .into()
        }),
        TextureMaps => thread_pool.spawn(async move {
            assemble_texturing_maps(&hubid.bundle_data_params(), tiles)
                .map(|[bkgrnd, overlay, blendcontrol]| {
                    vec![
                        TerrainData(hubid, bkgrnd),
                        TerrainData(hubid, overlay),
                        TerrainData(hubid, blendcontrol),
                    ]
                })
                .into()
        }),
        TintMap => thread_pool.spawn(async move {
            let params = hubid.bundle_data_params();
            assemble_tintmap(&params, tiles)
                .and_then(|data| decode_and_upscale_tintmap(&params, data))
                .map(|data| TerrainData(hubid, data))
                .into()
        }),
    };
    vec![AssembleMapTask(task)]
}
// ----------------------------------------------------------------------------
pub(in crate::extraction) fn split_map_into_sections(
    sections: &[(Witcher3HubId, MapSection)],
    input_data: &mut Vec<DataType>,
) -> Result<Vec<DataType>, String> {
    use DataType::*;

    let mapdata = input_data.drain(..).map(|d| match d {
        TerrainData(hubid, data) => (hubid, data),
        _ => unreachable!("wrong data type for SplitMap"),
    });

    let mut new_sections = Vec::new();

    for (hubid, mapdata) in mapdata {
        let params = hubid.bundle_data_params();
        new_sections.append(&mut splitmap(&params, mapdata, sections)?);
    }

    Ok(new_sections)
}
// ----------------------------------------------------------------------------
pub(in crate::extraction) fn generate_store_maps_tasks(
    thread_pool: &AsyncComputeTaskPool,
    destination_path: &std::path::Path,
    input_data: &mut Vec<DataType>,
) -> Vec<StoreMapTask> {
    let mut tasks = Vec::with_capacity(input_data.len());
    for data in input_data.drain(..) {
        match data {
            DataType::TerrainData(hubid, data) => {
                let path = destination_path.to_path_buf();

                let task = thread_pool.spawn(async move { storemap(&path, hubid, data).into() });
                tasks.push(StoreMapTask(task));
            }
            _ => unreachable!("wrong data type for store maps"),
        }
    }
    tasks
}
// ----------------------------------------------------------------------------
fn storemap(basepath: &Path, hubid: Witcher3HubId, data: TerrainData) -> Result<(), String> {
    use TerrainData::*;

    match data {
        Heightmap(data) => {
            let filepath = basepath.join(hubid.heightmap_filename());
            store_heightmap(&filepath, &data, hubid.map_size())?;
        }
        BackgroundTexturing(data) => {
            let filepath = basepath.join(hubid.texturemap_background_filename());
            store_texturing_map(&filepath, &data, hubid.map_size(), &TEXTURING_PALETTE)?;
        }
        OverlayTexturing(data) => {
            let filepath = basepath.join(hubid.texturemap_overlay_filename());
            store_texturing_map(&filepath, &data, hubid.map_size(), &TEXTURING_PALETTE)?;
        }
        BlendControl(data) => {
            let filepath = basepath.join(hubid.texturemap_control_filename());
            store_texturing_map(&filepath, &data, hubid.map_size(), &blendcontrol_palette())?;
        }
        Tintmap(data) => {
            let filepath = basepath.join(hubid.tintmap_filename());
            store_tintmap(&filepath, &data, hubid.map_size())?;
        }
    }
    Ok(())
}
// ----------------------------------------------------------------------------
fn assemble_heightmap(
    hub: &HubBundleDataParams,
    tiles: Vec<TileData>,
) -> Result<TerrainData, String> {
    let size = hub.res * hub.res * 2 * hub.x_tiles * hub.y_tiles;

    let mut result = utils::try_allocate_memory(size, "heightmap")?;
    let max_lines = hub.y_tiles * hub.res - 1;

    for TileData { x, y, data } in tiles {
        for line in 0..hub.res {
            for col in 0..hub.res {
                let max_width = hub.x_tiles * hub.res * 2;
                let targetpos =
                    max_width * (max_lines - (y * hub.res + line)) + (x * hub.res * 2) + col * 2;

                result[targetpos] = data[line * hub.res * 2 + col * 2 + 1];
                result[targetpos + 1] = data[line * hub.res * 2 + col * 2];
            }
        }
    }

    // vanilla data seems to be broken at borders: first line and last column of
    // heightmap seems to have a wrong height value. especially visible in bob
    // and kaer morhen heightmap.
    // Workaround:
    // copy 2nd line to 1st and also duplicate n-1 column
    let width = 2 * hub.res * hub.x_tiles;
    let height = hub.res * hub.x_tiles;
    result.copy_within(width..2 * width, 0);

    for y in 0..height {
        // Note u16 -> 2 bytes
        let start = y * width + width - 4;
        let end = start + 2;
        result.copy_within(start..end, end);
    }

    Ok(TerrainData::Heightmap(result))
}
// ----------------------------------------------------------------------------
fn assemble_texturing_maps(
    hub: &HubBundleDataParams,
    tiles: Vec<TileData>,
) -> Result<[TerrainData; 3], String> {
    use TerrainData::*;

    let size = hub.res * hub.res * hub.x_tiles * hub.y_tiles;

    let mut bkgrnd = utils::try_allocate_memory(size, "background texturing map")?;
    let mut overlay = utils::try_allocate_memory(size, "overlay texturing map")?;
    let mut blendcontrol = utils::try_allocate_memory(size, "blendcontrol map")?;
    let max_lines = hub.y_tiles * hub.res - 1;

    for TileData { x, y, data } in tiles {
        for line in 0..hub.res {
            for col in 0..hub.res {
                let max_width = hub.x_tiles * hub.res;
                let targetpos =
                    max_width * (max_lines - (y * hub.res + line)) + (x * hub.res) + col;

                let value: u16 = u16::from(data[line * hub.res * 2 + col * 2])
                    + u16::from(data[line * hub.res * 2 + col * 2 + 1]).shl(8);

                overlay[targetpos] = value as u8 & 0b_0001_1111;
                bkgrnd[targetpos] = (value & 0b_0000_0011_1110_0000).shr(5) as u8;
                blendcontrol[targetpos] = (value & 0b_1111_1100_0000_0000).shr(10) as u8;
            }
        }
    }

    Ok([
        BackgroundTexturing(bkgrnd),
        OverlayTexturing(overlay),
        BlendControl(blendcontrol),
    ])
}
// ----------------------------------------------------------------------------
fn assemble_tintmap(
    hub: &HubBundleDataParams,
    tiles: Vec<TileData>,
) -> Result<TerrainData, String> {
    let dds_compress_res = 4;
    let dds_pixeldata_size = 8;

    let tile_res = hub.res / 2_usize.pow(hub.colormap_start_mip) / dds_compress_res;
    let target_buffer_lines = hub.y_tiles * tile_res;
    let target_buffer_line_bytes = hub.x_tiles * tile_res * dds_pixeldata_size;

    let mut result =
        utils::try_allocate_memory(target_buffer_lines * target_buffer_line_bytes, "tintmap")?;

    for TileData { x, y, data } in tiles {
        for line in 0..tile_res {
            let targetpos = target_buffer_line_bytes * (y * tile_res + line)
                + x * tile_res * dds_pixeldata_size;

            let target_slice = &mut result[targetpos..targetpos + tile_res * dds_pixeldata_size];
            let src = &data
                [line * tile_res * dds_pixeldata_size..(line + 1) * tile_res * dds_pixeldata_size];

            target_slice.copy_from_slice(src);
        }
    }

    Ok(TerrainData::Tintmap(result))
}
// ----------------------------------------------------------------------------
fn decode_and_upscale_tintmap(
    hub: &HubBundleDataParams,
    data: TerrainData,
) -> Result<TerrainData, String> {
    use ddsfile::{Dds, NewDxgiParams};

    use image::{DynamicImage::ImageRgb8, ImageBuffer, ImageDecoder};

    if let TerrainData::Tintmap(data) = data {
        // -- create dds header and prepend to data
        let tile_res = hub.res / 2_usize.pow(hub.colormap_start_mip);
        let width = (tile_res * hub.x_tiles) as u32;
        let height = (tile_res * hub.x_tiles) as u32;

        let mut dds = Dds::new_dxgi(NewDxgiParams {
            height,
            width,
            depth: None,
            format: ddsfile::DxgiFormat::BC1_UNorm,
            mipmap_levels: None,
            array_layers: None,
            caps2: None,
            is_cubemap: false,
            resource_dimension: ddsfile::D3D10ResourceDimension::Texture2D,
            alpha_mode: ddsfile::AlphaMode::Unknown,
        })
        .map_err(|e| format!("DDS parser: {}", e))?;

        let mut ddsfile = Vec::with_capacity(data.len() + 256); // ~256 for header size
        dds.data = data;
        dds.write(&mut ddsfile).expect("ddswrite");

        // -- decode dds
        // RGB
        let mut decoded = utils::try_allocate_memory(
            tile_res * tile_res * hub.x_tiles * hub.y_tiles * 3,
            "decoded dds tint buffer",
        )?;

        let dds_decoder = image::codecs::dds::DdsDecoder::new(ddsfile.as_slice())
            .map_err(|e| format!("could not decode dds: {}", e))?;

        dds_decoder
            .read_image(&mut decoded)
            .map_err(|e| format!("could not read decoded dds image: {}", e))?;

        // --- upscale to full hub res (required for hub encoder)
        let img = ImageRgb8(ImageBuffer::from_raw(width, height, decoded).unwrap()).flipv();
        let new_width = (hub.res * hub.x_tiles) as u32;
        let new_height = (hub.res * hub.y_tiles) as u32;

        let upscaled = img.resize(new_width, new_height, image::imageops::FilterType::Triangle);
        Ok(TerrainData::Tintmap(upscaled.into_rgba8().into_raw()))
    } else {
        panic!("decode_and_upscale_tintmap requires tintmap as input");
    }
}
// ----------------------------------------------------------------------------
fn splitmap(
    hub: &HubBundleDataParams,
    src_data: TerrainData,
    sections: &[(Witcher3HubId, MapSection)],
) -> Result<Vec<DataType>, String> {
    use TerrainData::*;

    let size = (hub.res * hub.x_tiles) as u32;

    let (src, px_size) = match &src_data {
        Heightmap(data) => (data, 2),
        BackgroundTexturing(data) => (data, 1),
        OverlayTexturing(data) => (data, 1),
        BlendControl(data) => (data, 1),
        Tintmap(data) => (data, 4),
    };

    let mut result = Vec::new();

    fn copy_rectangle(
        src: &[u8],
        src_size: u32,
        section: &MapSection,
        px_size: u32,
    ) -> Result<Vec<u8>, String> {
        let mut buf = utils::try_allocate_memory(
            (px_size * section.size * section.size) as usize,
            "target buffer for map section",
        )?;

        let src_line_size = (src_size * px_size) as usize;
        let target_line_size = (section.size * px_size) as usize;

        for y in 0..section.size {
            let src_offset =
                (section.pos.1 + y) as usize * src_line_size + (section.pos.0 * px_size) as usize;

            let target_offset = y as usize * target_line_size;

            buf[target_offset..target_offset + target_line_size]
                .copy_from_slice(&src[src_offset..src_offset + target_line_size]);
        }

        Ok(buf)
    }

    for (new_hubid, section) in sections {
        if section.pos.0 + section.size > size || section.pos.1 + section.size > size {
            return Err(format!(
                "extract section from map failed: section {:?} outside mapsize {}",
                section, size
            ));
        }

        let section = copy_rectangle(src, size, section, px_size)?;

        let wrapped = match src_data {
            Heightmap(_) => Heightmap(section),
            BackgroundTexturing(_) => BackgroundTexturing(section),
            OverlayTexturing(_) => OverlayTexturing(section),
            BlendControl(_) => BlendControl(section),
            Tintmap(_) => Tintmap(section),
        };
        result.push(DataType::TerrainData(*new_hubid, wrapped));
    }

    Ok(result)
}
// ----------------------------------------------------------------------------
// storing data
// ----------------------------------------------------------------------------
fn store_heightmap(filepath: &Path, data: &[u8], size: u32) -> Result<(), String> {
    utils::store_image(
        filepath,
        data,
        size,
        size,
        ColorType::Grayscale,
        BitDepth::Sixteen,
        Compression::Best,
        None,
    )
}
// ----------------------------------------------------------------------------
fn store_texturing_map(
    filepath: &Path,
    data: &[u8],
    size: u32,
    texture_palette: &[u8],
) -> Result<(), String> {
    utils::store_image(
        filepath,
        data,
        size,
        size,
        ColorType::Indexed,
        BitDepth::Eight,
        Compression::Default,
        Some(texture_palette),
    )
}
// ----------------------------------------------------------------------------
fn store_tintmap(filepath: &Path, data: &[u8], size: u32) -> Result<(), String> {
    utils::store_image(
        filepath,
        data,
        size,
        size,
        ColorType::Rgba,
        BitDepth::Eight,
        Compression::Default,
        None,
    )
}
// ----------------------------------------------------------------------------
// helper
// ----------------------------------------------------------------------------
impl Witcher3HubId {
    // ------------------------------------------------------------------------
    const fn bundle_data_params(&self) -> HubBundleDataParams {
        use Witcher3HubId::*;

        match self {
            Novigrad | Velen => HubBundleDataParams::new("\\novigrad\\", 46, 512, 2),
            Skellige => HubBundleDataParams::new("\\skellige\\", 32, 512, 2),
            KaerMorhen => HubBundleDataParams::new("\\kaer_morhen\\", 16, 1024, 2),
            Prologue => HubBundleDataParams::new("\\prolog_village\\", 16, 256, 0),
            PrologueWinter => HubBundleDataParams::new("\\prolog_village_winter\\", 16, 256, 0),
            Vizima => HubBundleDataParams::new("\\wyzima_castle\\", 9, 512, 0),
            IsleOfMist => HubBundleDataParams::new("\\island_of_mist\\", 4, 1024, 0),
            SpiralDesert | SpiralDarkValley | SpiralElvenCity | SpiralSnowValley => {
                HubBundleDataParams::new("\\the_spiral\\", 24, 1024, 2)
            }
            Bob => HubBundleDataParams::new("\\bob\\", 32, 512, 2),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl HubBundleDataParams {
    // ------------------------------------------------------------------------
    const fn new(
        directory: &'static str,
        tiles: usize,
        res: usize,
        colormap_start_mip: u32,
    ) -> Self {
        Self {
            directory,
            res,
            x_tiles: tiles,
            y_tiles: tiles,
            colormap_start_mip,
        }
    }
    // ------------------------------------------------------------------------
    const fn heightmap_buffer(&self) -> usize {
        1
    }
    // ------------------------------------------------------------------------
    const fn texturemap_buffer(&self) -> usize {
        2
    }
    // ------------------------------------------------------------------------
    const fn tintmap_buffer(&self) -> usize {
        self.colormap_start_mip as usize * 2 + 3
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl MapSection {
    // ------------------------------------------------------------------------
    pub fn new(x: u32, y: u32, size: u32) -> Self {
        Self { pos: (x, y), size }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
type TexturePalette = [u8; 32 * 3];

#[rustfmt::skip]
const TEXTURING_PALETTE: TexturePalette = [
    0, 0, 0,        75, 87, 66,     68, 82, 61,
    102, 88, 75,    81, 73, 62,     74, 92, 59,
    81, 70, 57,     70, 62, 54,     85, 73, 64,
    70, 68, 54,     66, 58, 51,     110, 99, 84,
    121, 113, 102,  105, 90, 75,    92, 112, 75,
    81, 102, 66,    90, 70, 59,     53, 62, 40,
    115, 92, 72,    90, 78, 64,     113, 104, 90,
    114, 115, 117,  105, 101, 97,   145, 143, 139,
    105, 97, 87,    151, 146, 132,  185, 172, 152,
    171, 164, 148,  182, 179, 175,  60, 79, 53,
    104, 105, 103,  36, 30, 22,
];
// ----------------------------------------------------------------------------
fn blendcontrol_palette() -> [u8; 64 * 3] {
    let mut palette = [0u8; 64 * 3];

    for i in 0..64 {
        let scale = (i % 8) as u8;
        let slope = (i / 8) as u8 % 8;
        palette[i * 3] = 32 + 255 / 8 * scale;
        palette[i * 3 + 1] = 55 + scale * slope * 4;
        palette[i * 3 + 2] = 32 + 255 / 8 * slope;
    }
    palette
}
// ----------------------------------------------------------------------------
// captions for progress msg
// ----------------------------------------------------------------------------
impl std::fmt::Display for ExtractionType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ExtractionType::Heightmap => write!(f, "heightmap"),
            ExtractionType::TextureMaps => write!(f, "texturing map"),
            ExtractionType::TintMap => write!(f, "tintmap"),
        }
    }
}
// ----------------------------------------------------------------------------

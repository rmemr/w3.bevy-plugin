// ----------------------------------------------------------------------------
use bevy::tasks::AsyncComputeTaskPool;

use ddsfile::{Dds, NewDxgiParams};

use crate::Witcher3HubId;

use super::reader::{Dimensions, LayerCount, MipCount, TextureFormat};

use super::textures::store_dds_as_png;
use super::textures::TextureType;
//
use super::{DataType, ExtractArrayLayerTask, StoreTerrainTextureTask};
// ----------------------------------------------------------------------------
pub(super) use super::reader::Texture as TextureData;
// ----------------------------------------------------------------------------
pub(in crate::extraction) fn extract_texturearray_layer(
    thread_pool: &AsyncComputeTaskPool,
    hubid: Witcher3HubId,
    input_data: &mut Vec<DataType>,
) -> Vec<ExtractArrayLayerTask> {
    use self::TextureData::Array;
    use DataType::*;

    let texture_arrays = input_data.drain(..).map(|d| match d {
        TextureData(Array(format, size, mips, slices, data)) => (format, size, mips, slices, data),
        _ => unreachable!("wrong data type for extract array slices task"),
    });
    let mut tasks = Vec::with_capacity(texture_arrays.len());

    for (format, size, mips, slices, data) in texture_arrays {
        let task = thread_pool.spawn(async move {
            extract_terrain_texture_layer(hubid, format, size, mips, slices, data).into()
        });

        tasks.push(ExtractArrayLayerTask(task));
    }
    tasks
}
// ----------------------------------------------------------------------------
pub(in crate::extraction) fn store_terraintextures(
    thread_pool: &AsyncComputeTaskPool,
    destination_path: &std::path::Path,
    texture_type: TextureType,
    input_data: &mut Vec<DataType>,
) -> Vec<StoreTerrainTextureTask> {
    let mut tasks = Vec::with_capacity(input_data.len());
    for data in input_data.drain(..) {
        match data {
            DataType::TerrainTextureDds(hubid, layer, dds) => {
                let filepath = match texture_type {
                    TextureType::Diffuse => hubid.material_path_diffuse(layer, "png"),
                    TextureType::Normal => hubid.material_path_normal(layer, "png"),
                };
                let filepath = destination_path.join(filepath);

                let task = thread_pool
                    .spawn(async move { store_dds_as_png(&filepath, dds, texture_type).into() });
                tasks.push(StoreTerrainTextureTask(task));
            }
            _ => unreachable!("wrong data type for store terrain textures"),
        }
    }
    tasks
}
// ----------------------------------------------------------------------------
fn extract_terrain_texture_layer(
    hubid: Witcher3HubId,
    format: TextureFormat,
    size: Dimensions,
    _mips: MipCount,
    layers: LayerCount,
    data: Vec<u8>,
) -> Result<Vec<DataType>, String> {
    let format = format.into();

    let mut main_texture = Dds::new_dxgi(NewDxgiParams {
        height: size.height as u32,
        width: size.width as u32,
        depth: None,
        format,
        // mipmap_levels: if *mips > 0 { Some(*mips as u32) } else { None },
        // Hack: unfortunately adding used miplevel count breaks layer extraction
        mipmap_levels: None,
        array_layers: if *layers > 0 {
            Some(*layers as u32)
        } else {
            None
        },
        caps2: None,
        is_cubemap: false,
        resource_dimension: ddsfile::D3D10ResourceDimension::Texture2D,
        alpha_mode: ddsfile::AlphaMode::Unknown,
    })
    .map_err(|e| format!("DDS parser: {}", e))?;

    main_texture.data = data;

    let mut result = Vec::with_capacity(*layers as usize);

    for layer in 0..*layers {
        let mut dds_layer = Dds::new_dxgi(NewDxgiParams {
            height: size.height as u32,
            width: size.width as u32,
            depth: None,
            format,
            mipmap_levels: None, // mip level extraction with ddsfile seems broken?
            array_layers: None,
            caps2: None,
            is_cubemap: false,
            resource_dimension: ddsfile::D3D10ResourceDimension::Texture2D,
            alpha_mode: ddsfile::AlphaMode::Unknown,
        })
        .map_err(|e| format!("DDS parser for layer {} failed: {}", layer, e))?;

        dds_layer.data = main_texture
            .get_data(layer as u32)
            .map_err(|e| format!("failed to extract data for layer {}: {}", layer, e))?
            .to_vec();

        result.push(DataType::TerrainTextureDds(hubid, layer as u8, dds_layer))
    }
    Ok(result)
}
// ----------------------------------------------------------------------------
// helper
// ----------------------------------------------------------------------------
impl Witcher3HubId {
    // ------------------------------------------------------------------------
    pub(super) const fn diffuse_texturearray_path(&self) -> &str {
        use Witcher3HubId::*;
        match self {
            Prologue => "levels\\prolog_village\\prolog_village.texarray",
            PrologueWinter => "levels\\prolog_village_winter\\prolog_village.texarray",
            Skellige | IsleOfMist | Vizima => "levels\\skellige\\skellige_terrain_atlas.texarray",
            Novigrad | Velen => "levels\\novigrad\\novigrad_array.texarray",
            KaerMorhen => "levels\\kaer_morhen\\kaer_morhen_valley.texarray",
            SpiralDarkValley | SpiralDesert | SpiralElvenCity | SpiralSnowValley => {
                "levels\\the_spiral\\spiral_terrain_array.texarray"
            }
            Bob => "dlc\\bob\\data\\levels\\bob\\bob_array.texarray",
        }
    }
    // ------------------------------------------------------------------------
    pub(super) const fn normal_texturearray_path(&self) -> &str {
        use Witcher3HubId::*;
        match self {
            Prologue => "levels\\prolog_village\\prolog_village_normals.texarray",
            PrologueWinter => "levels\\prolog_village_winter\\prolog_village_normals.texarray",
            Skellige | IsleOfMist | Vizima => {
                "levels\\skellige\\skellige_terrain_normal_atlas.texarray"
            }
            Novigrad | Velen => "levels\\novigrad\\novigrad_array_n.texarray",
            KaerMorhen => "levels\\kaer_morhen\\kaer_morhen_valley_normals.texarray",

            SpiralDarkValley | SpiralDesert | SpiralElvenCity | SpiralSnowValley => {
                "levels\\the_spiral\\spiral_terrain_array_n.texarray"
            }
            Bob => "dlc\\bob\\data\\levels\\bob\\bob_array_n.texarray",
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

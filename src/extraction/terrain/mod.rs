// ----------------------------------------------------------------------------
use crate::extraction::progress::{RequestProgress, TaskId, TaskProgress};
use crate::extraction::ProgressTrackingState;
use crate::Witcher3HubId;

use super::reader;
use super::textures;
use super::{
    AssembleMapTask, BundleExtractionTask, DataType, ExtractArrayLayerTask,
    ExtractBundleFilesRequest, ExtractionRequest, ExtractionTask, StoreMapTask,
    StoreTerrainTextureTask,
};

// ----------------------------------------------------------------------------
pub(super) use self::data::{ExtractionType, MapSection, TerrainData, TileData};
// ----------------------------------------------------------------------------
pub(super) use self::material::extract_texturearray_layer;
pub(super) use self::material::store_terraintextures;
// ----------------------------------------------------------------------------
pub(super) use self::data::assemble_map;
pub(super) use self::data::generate_store_maps_tasks;
pub(super) use self::data::split_map_into_sections;
// ----------------------------------------------------------------------------
mod data;
mod material;
// ----------------------------------------------------------------------------
// definition of required tasks for terrain hub data extraction
// ----------------------------------------------------------------------------
struct ExtractTerrainData(Witcher3HubId, ExtractionType);
// ----------------------------------------------------------------------------
impl Witcher3HubId {
    // ------------------------------------------------------------------------
    pub(super) fn terrain_map_extraction_tasks(
        &self,
    ) -> (ProgressTrackingState, Vec<(ExtractionTask, TaskProgress)>) {
        use Witcher3HubId::*;

        match self {
            Prologue => Self::generate_tasks(Prologue, 16 * 16),
            PrologueWinter => Self::generate_tasks(PrologueWinter, 16 * 16),
            Skellige => Self::generate_tasks(Skellige, 16 * 16),
            KaerMorhen => Self::generate_tasks(KaerMorhen, 16 * 16),
            IsleOfMist => Self::generate_tasks(IsleOfMist, 16 * 16),
            Bob => Self::generate_tasks(Bob, 16 * 16),
            // following maps require splitting into multiple sections or
            // picking a 2^k rectangle
            Novigrad | Velen => Self::generate_splitmap_tasks(
                Novigrad,
                46 * 46,
                vec![
                    (Novigrad, MapSection::new(7168, 0, 16384)),
                    (Velen, MapSection::new(0, 7168, 16384)),
                ],
            ),
            Vizima => Self::generate_splitmap_tasks(
                Vizima,
                9 * 9,
                vec![(Vizima, MapSection::new(264, 206, 4096))],
            ),
            SpiralDarkValley | SpiralDesert | SpiralElvenCity | SpiralSnowValley => {
                Self::generate_splitmap_tasks(
                    SpiralSnowValley,
                    24 * 24,
                    vec![
                        (SpiralSnowValley, MapSection::new(8190, 8190, 16384)),
                        (SpiralDarkValley, MapSection::new(8100, 17340, 4096)),
                        (SpiralDesert, MapSection::new(1050, 16045, 8192)),
                        (SpiralElvenCity, MapSection::new(8190, 2, 16384)),
                    ],
                )
            }
        }
    }
    // ------------------------------------------------------------------------
    fn generate_tasks(
        hubid: Self,
        tiles: usize,
    ) -> (ProgressTrackingState, Vec<(ExtractionTask, TaskProgress)>) {
        use ExtractionType::*;

        let progress_tracking = RequestProgress::new(ExtractionRequest::TerrainData(hubid))
            // heightmap
            .add_task(TaskId("ExtractBundleFiles"), tiles)
            .add_task(TaskId("AssembleMap"), 1)
            .add_task(TaskId("StoreMaps"), 1)
            // tintmap
            .add_task(TaskId("ExtractBundleFiles"), tiles)
            .add_task(TaskId("AssembleMap"), 1)
            .add_task(TaskId("StoreMaps"), 1)
            // texturemap
            .add_task(TaskId("ExtractBundleFiles"), tiles)
            .add_task(TaskId("AssembleMap"), 3)
            .add_task(TaskId("StoreMaps"), 3);
        let start_state = progress_tracking.start_state();

        let tasks = vec![
            ExtractionTask::ExtractBundleFiles(Box::new(ExtractTerrainData(hubid, Heightmap))),
            ExtractionTask::AssembleMap(hubid, Heightmap),
            ExtractionTask::StoreMaps,
            ExtractionTask::ExtractBundleFiles(Box::new(ExtractTerrainData(hubid, TintMap))),
            ExtractionTask::AssembleMap(hubid, TintMap),
            ExtractionTask::StoreMaps,
            ExtractionTask::ExtractBundleFiles(Box::new(ExtractTerrainData(hubid, TextureMaps))),
            ExtractionTask::AssembleMap(hubid, TextureMaps),
            ExtractionTask::StoreMaps,
        ]
        .drain(..)
        .zip(progress_tracking.into_tasks())
        .collect::<Vec<_>>();

        (start_state, tasks)
    }
    // ------------------------------------------------------------------------
    fn generate_splitmap_tasks(
        hubid: Self,
        tiles: usize,
        sections: Vec<(Witcher3HubId, MapSection)>,
    ) -> (ProgressTrackingState, Vec<(ExtractionTask, TaskProgress)>) {
        use ExtractionType::*;

        let progress_tracking = RequestProgress::new(ExtractionRequest::TerrainData(hubid))
            // heightmap
            .add_task(TaskId("ExtractBundleFiles"), tiles)
            .add_task(TaskId("AssembleMap"), 1)
            .add_task(TaskId("SplitMap"), 1)
            .add_task(TaskId("StoreMaps"), sections.len())
            // tintmap
            .add_task(TaskId("ExtractBundleFiles"), tiles)
            .add_task(TaskId("AssembleMap"), 1)
            .add_task(TaskId("SplitMap"), 1)
            .add_task(TaskId("StoreMaps"), sections.len())
            // texturemap
            .add_task(TaskId("ExtractBundleFiles"), tiles)
            .add_task(TaskId("AssembleMap"), 3)
            .add_task(TaskId("SplitMap"), 1)
            .add_task(TaskId("StoreMaps"), 3 * sections.len());

        let start_state = progress_tracking.start_state();

        let tasks = vec![
            ExtractionTask::ExtractBundleFiles(Box::new(ExtractTerrainData(hubid, Heightmap))),
            ExtractionTask::AssembleMap(hubid, Heightmap),
            ExtractionTask::SplitMap(sections.clone()),
            ExtractionTask::StoreMaps,
            ExtractionTask::ExtractBundleFiles(Box::new(ExtractTerrainData(hubid, TintMap))),
            ExtractionTask::AssembleMap(hubid, TintMap),
            ExtractionTask::SplitMap(sections.clone()),
            ExtractionTask::StoreMaps,
            ExtractionTask::ExtractBundleFiles(Box::new(ExtractTerrainData(hubid, TextureMaps))),
            ExtractionTask::AssembleMap(hubid, TextureMaps),
            ExtractionTask::SplitMap(sections),
            ExtractionTask::StoreMaps,
        ]
        .drain(..)
        .zip(progress_tracking.into_tasks())
        .collect::<Vec<_>>();

        (start_state, tasks)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// definition of required tasks for terrain texture extraction
// ----------------------------------------------------------------------------
impl Witcher3HubId {
    // ------------------------------------------------------------------------
    pub(in crate::extraction) fn terrain_textures_extraction_tasks(
        &self,
    ) -> (ProgressTrackingState, Vec<(ExtractionTask, TaskProgress)>) {
        use Witcher3HubId::*;

        match self {
            Prologue => Self::generate_texture_tasks(Prologue),
            PrologueWinter => Self::generate_texture_tasks(PrologueWinter),
            Skellige | IsleOfMist | Vizima => Self::generate_texture_tasks(Skellige),
            Novigrad | Velen => Self::generate_texture_tasks(Novigrad),
            KaerMorhen => Self::generate_texture_tasks(KaerMorhen),
            SpiralDarkValley | SpiralDesert | SpiralElvenCity | SpiralSnowValley => {
                Self::generate_texture_tasks(SpiralSnowValley)
            }
            Bob => Self::generate_texture_tasks(Bob),
        }
    }
    // ------------------------------------------------------------------------
    fn generate_texture_tasks(
        hubid: Self,
    ) -> (ProgressTrackingState, Vec<(ExtractionTask, TaskProgress)>) {
        let texture_count = hubid.terrain_texture_count() as usize;

        let progress_tracking = RequestProgress::new(ExtractionRequest::TerrainTextures(hubid))
            // diffuse
            .add_task(TaskId("ExtractTextureArray"), 1)
            .add_task(TaskId("ExtractTextureArrayLayer"), 1)
            .add_task(TaskId("StoreTerrainTextures"), texture_count)
            // normals
            .add_task(TaskId("ExtractTextureArray"), 1)
            .add_task(TaskId("ExtractTextureArrayLayer"), 1)
            .add_task(TaskId("StoreTerrainTextures"), texture_count);

        let start_state = progress_tracking.start_state();

        let tasks = vec![
            // diffuse
            ExtractionTask::ExtractTextureFiles(Box::new(textures::ExtractTextureData(
                hubid.diffuse_texturearray_path().into(),
            ))),
            ExtractionTask::ExtractTextureArrayLayer(hubid),
            ExtractionTask::StoreTerrainTextures(textures::TextureType::Diffuse),
            // normals
            ExtractionTask::ExtractTextureFiles(Box::new(textures::ExtractTextureData(
                hubid.normal_texturearray_path().into(),
            ))),
            ExtractionTask::ExtractTextureArrayLayer(hubid),
            ExtractionTask::StoreTerrainTextures(textures::TextureType::Normal),
        ]
        .drain(..)
        .zip(progress_tracking.into_tasks())
        .collect::<Vec<_>>();

        (start_state, tasks)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
use std::fs;
use std::hash::Hash;
use std::path::{Path, PathBuf};

use bevy::{prelude::*, utils::HashMap};

pub use bundle::BundleRegistry;
pub use texturecache::{
    Dimensions, MipCount, LayerCount, Texture, TextureCacheRegistry, TextureFormat,
};
// ----------------------------------------------------------------------------
mod doboz;

mod bundle;
mod texturecache;
mod utils;
// ----------------------------------------------------------------------------
pub struct ItemCollection<Item: Clone> {
    container: HashMap<ContainerId, PathBuf>,
    items: HashMap<String, (ContainerId, Item)>,
}
// ----------------------------------------------------------------------------
pub trait ItemDataReader<D> {
    fn read_data(&self, item: &str) -> Result<D, String>;
}
// ----------------------------------------------------------------------------
struct Registry<C: Container> {
    container: HashMap<ContainerId, ContainerInfo<C::Item>>,
}
// ----------------------------------------------------------------------------
#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
struct ContainerId(usize);
// ----------------------------------------------------------------------------
struct ContainerInfo<Item: ContainerItemInfo> {
    path: PathBuf,
    items: Vec<Item>,
}
// ----------------------------------------------------------------------------
trait ContainerItemInfo: Clone + std::fmt::Debug {
    fn name(&self) -> &str;
}
// ----------------------------------------------------------------------------
trait Container: Sized
where
    Self::Item: ContainerItemInfo,
{
    type Item;
    // ------------------------------------------------------------------------
    fn read_info(path: &Path, filter: Option<&str>) -> Result<ContainerInfo<Self::Item>, String>;
    // ------------------------------------------------------------------------
    fn file_extension() -> &'static str;
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<C: Container> Registry<C> {
    // ------------------------------------------------------------------------
    fn init(&mut self, game_path: &Path, filter: Option<&str>) -> Result<(), String> {
        use walkdir::{DirEntry, WalkDir};

        // scan for all container files and read their item registries
        info!("> scanning for files in {}", game_path.display());

        let game_path = game_path.canonicalize().map_err(|e| e.to_string())?;

        let is_container_file = |entry: &DirEntry| -> bool {
            entry
                .file_name()
                .to_str()
                .map(|s| s.ends_with(C::file_extension()))
                .unwrap_or(false)
        };

        let container_filelist = WalkDir::new(&game_path)
            .contents_first(true)
            .into_iter()
            .filter_entry(|e| e.path().is_dir() || is_container_file(e))
            .collect::<Result<Vec<_>, _>>()
            .map_err(|e| format!("read error: {}", e))?;

        for file in container_filelist.iter().filter(|e| e.path().is_file()) {
            let metadata = fs::metadata(file.path()).map_err(|e| {
                format!(
                    "failed to extract metadata for file {}: {}",
                    file.path().display(),
                    e
                )
            })?;

            if metadata.is_file() && metadata.len() > 0 {
                let id = ContainerId(self.container.len());
                debug!("loading item info from {}...", file.path().display());

                match C::read_info(file.path(), filter) {
                    Ok(container_info) => {
                        if !container_info.items.is_empty() {
                            let pretty_path = file
                                .path()
                                .strip_prefix(&game_path)
                                .unwrap_or_else(|_| file.path());

                            info!(
                                "found {:>5} data chunks in {}",
                                container_info.items.len(),
                                pretty_path.display()
                            );
                            self.container.insert(id, container_info);
                        }
                    }
                    Err(e) => {
                        error!("failure reading file: {}", e);
                    }
                }
            }
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
    fn filter<F: Fn(&str) -> bool>(&self, filter: F) -> ItemCollection<C::Item> {
        let mut filtered = ItemCollection::default();

        for (id, container) in self.container.iter() {
            let items = container
                .items
                .iter()
                .filter(|item| filter(item.name()))
                .cloned()
                .collect::<Vec<_>>();

            if !items.is_empty() {
                let is_patch = container.path.to_string_lossy().contains("patch");

                filtered.container.insert(*id, container.path.clone());
                for item in items {
                    if !filtered.items.contains_key(item.name()) || is_patch {
                        filtered.items.insert(item.name().to_string(), (*id, item));
                    }
                }
            }
        }
        filtered
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<Item: Clone> ItemCollection<Item> {
    // ------------------------------------------------------------------------
    pub fn shorten_names(mut self) -> Self {
        self.items = self
            .items
            .drain()
            .map(|(name, value)| {
                let name = match name.rsplit('\\').next() {
                    Some(part) => part.to_string(),
                    None => name,
                };

                (name, value)
            })
            .collect();
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// defaults
// ----------------------------------------------------------------------------
impl<C: Container> Default for Registry<C> {
    fn default() -> Self {
        Self {
            container: HashMap::default(),
        }
    }
}
// ----------------------------------------------------------------------------
impl<Item: Clone> Default for ItemCollection<Item> {
    fn default() -> Self {
        Self {
            container: HashMap::default(),
            items: HashMap::default(),
        }
    }
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
use std::cmp::Ordering;
use std::fmt::{Binary, Display, LowerHex};
use std::io::Read;
use std::mem;

use bevy::prelude::*;
use byteorder::{LittleEndian, ReadBytesExt};
// ----------------------------------------------------------------------------
#[rustfmt::skip]
pub(super) trait ReadHelper {
    fn read_exact(&mut self, buf: &mut [u8], name: &str) -> Result<(), String>;
    fn read_u8(&mut self, name: &str) -> Result<u8, String>;
    fn read_u16(&mut self, name: &str) -> Result<u16, String>;
    fn read_u32(&mut self, name: &str) -> Result<u32, String>;
    fn read_u64(&mut self, name: &str) -> Result<u64, String>;
    fn read_fixed_bufsize_zero_terminated_string(&mut self, bufsize: usize, name: &str) -> Result<String, String>;
    fn read_zero_terminated_string(&mut self, max_len: usize, name: &str) -> Result<String, String>;
}
// ----------------------------------------------------------------------------
impl<T: Read + ReadBytesExt> ReadHelper for T {
    // ------------------------------------------------------------------------
    fn read_exact(&mut self, buf: &mut [u8], name: &str) -> Result<(), String> {
        <Self as Read>::read_exact(self, buf)
            .map_err(|e| format!("failed to read [{}]: {}", name, e))
    }
    // ------------------------------------------------------------------------
    fn read_u8(&mut self, name: &str) -> Result<u8, String> {
        <Self as ReadBytesExt>::read_u8(self)
            .map_err(|e| format!("failed to read [{}]: {}", name, e))
    }
    // ------------------------------------------------------------------------
    fn read_u16(&mut self, name: &str) -> Result<u16, String> {
        <Self as ReadBytesExt>::read_u16::<LittleEndian>(self)
            .map_err(|e| format!("failed to read [{}]: {}", name, e))
    }
    // ------------------------------------------------------------------------
    fn read_u32(&mut self, name: &str) -> Result<u32, String> {
        <Self as ReadBytesExt>::read_u32::<LittleEndian>(self)
            .map_err(|e| format!("failed to read [{}]: {}", name, e))
    }
    // ------------------------------------------------------------------------
    fn read_u64(&mut self, name: &str) -> Result<u64, String> {
        <Self as ReadBytesExt>::read_u64::<LittleEndian>(self)
            .map_err(|e| format!("failed to read [{}]: {}", name, e))
    }
    // ------------------------------------------------------------------------
    #[rustfmt::skip]
    fn read_fixed_bufsize_zero_terminated_string(&mut self, bufsize: usize, name: &str) -> Result<String, String> {
        let mut buf = vec![0; bufsize];
        <Self as ReadHelper>::read_exact(self, &mut buf, name)?;

        let len = match buf.iter().position(|&x| 0 == x) {
            Some(n) => n,
            None => bufsize,
        };
        buf.truncate(len);

        Ok(String::from_utf8_lossy(&buf).to_string())
    }
    // ------------------------------------------------------------------------
    #[rustfmt::skip]
    fn read_zero_terminated_string(&mut self, max_len: usize, name: &str) -> Result<String, String> {
        let mut buf = Vec::with_capacity(max_len);

        for _ in 0..max_len {
            match self.read_u8() {
                Ok(value) if value == 0 => {
                    return Ok(String::from_utf8_lossy(&buf).to_string());
                }
                Ok(value) => {
                    buf.push(value);
                }
                Err(e) => {
                    return Err(format!("failed to read [{}]: {}", name, e));
                }
            }
        }
        Err(format!("failed to read [{}]: exceeded max len {}", name, max_len))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
pub(super) trait VerifyHelper {
    // ------------------------------------------------------------------------
    fn verify_unknown<T>(data: T, expected: T, typeguess: &'static str) -> Result<(), String>
    where
        T: Ord + LowerHex + Binary + Display,
    {
        let bytes = mem::size_of::<T>();

        if data.cmp(&expected) == Ordering::Equal {
            Ok(())
        } else {
            error!(
                "expected           (unknown) 0x{:04$x} ({:05$b}) ({:06$}) ({})",
                expected,
                expected,
                expected,
                typeguess,
                bytes * 2,
                bytes * 8,
                bytes * 3
            );
            Err(format!(
                "but got 0x{:04$x} ({:05$b}) ({:06$}) ({})",
                data,
                data,
                data,
                typeguess,
                bytes * 2,
                bytes * 8,
                bytes * 3
            ))
        }
    }
    // ------------------------------------------------------------------------
    fn verify_known<T>(data: T, expected: T, datatype: &'static str) -> Result<(), String>
    where
        T: Ord + LowerHex + Binary + Display,
    {
        if data.cmp(&expected) == Ordering::Equal {
            Ok(())
        } else {
            Err(format!(
                "expected {}: {} but got {}",
                datatype, expected, data
            ))
        }
    }
    // ------------------------------------------------------------------------
    fn log_unknown<T>(data: T, typeguess: &'static str)
    where
        T: LowerHex + Binary + Display,
    {
        let bytes = std::mem::size_of::<T>();

        warn!(
            "unknown:                               0x{:04$x} ({:05$b}) ({}) ({})",
            data,
            data,
            data,
            typeguess,
            bytes * 2,
            bytes * 8
        );
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

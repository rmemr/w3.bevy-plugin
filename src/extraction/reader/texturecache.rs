// ----------------------------------------------------------------------------
use std::fs::File;
use std::io::{BufReader, Read, Seek, SeekFrom};
use std::path::Path;

use crate::utils;

use bevy::prelude::*;

use super::{
    Container, ContainerInfo, ContainerItemInfo, ItemCollection, ItemDataReader, Registry,
};
// ----------------------------------------------------------------------------
/// NewType wrapper to reduce exposed traits and internal datatypes (e.g. CacheItem)
#[derive(Default)]
pub struct TextureCacheRegistry(Registry<CacheItem>);
// ----------------------------------------------------------------------------
impl TextureCacheRegistry {
    // ------------------------------------------------------------------------
    pub fn init(&mut self, game_path: &Path, filter: Option<&str>) -> Result<(), String> {
        self.0.init(game_path, filter)
    }
    // ------------------------------------------------------------------------
    pub fn filter<F: Fn(&str) -> bool>(&self, filter: F) -> ItemCollection<CacheItem> {
        self.0.filter(filter)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
#[derive(Deref, Debug)]
pub struct MipCount(u16);
// ----------------------------------------------------------------------------
#[derive(Deref, Debug)]
pub struct LayerCount(u16);
// ----------------------------------------------------------------------------
#[derive(Debug)]
pub struct Dimensions {
    pub width: u16,
    pub height: u16,
}
// ----------------------------------------------------------------------------
#[derive(Debug, Copy, Clone)]
pub enum TextureFormat {
    Bc1Unorm, // BC1_UNorm_sRGB ?
    Bc3Unorm, // BC3_Unorm_sRGB ?
    Unsupported(u8, u8),
}
// ----------------------------------------------------------------------------
pub enum Texture {
    Image(TextureFormat, Dimensions, MipCount, Vec<u8>),
    Array(TextureFormat, Dimensions, MipCount, LayerCount, Vec<u8>),
    CubeMap(TextureFormat, Dimensions, MipCount, LayerCount, Vec<u8>),
}
// ----------------------------------------------------------------------------
const TEXTURE_CACHE_FOOTER_SIZE: u32 = 32;
const TEXTURE_CACHE_MIPMAP_OFFSET_SIZE: u32 = 4;
const TEXTURE_CACHE_ITEM_ENTRY_SIZE: u32 = 52;
// ----------------------------------------------------------------------------
impl Container for CacheItem {
    type Item = CacheItem;
    // ------------------------------------------------------------------------
    fn file_extension() -> &'static str {
        "texture.cache"
    }
    // ------------------------------------------------------------------------
    fn read_info(path: &Path, filter: Option<&str>) -> Result<ContainerInfo<Self::Item>, String> {
        let mut file = File::open(path)
            .map(BufReader::new)
            .map_err(|e| format!("failed to open {}: {}", path.display(), e))?;

        file.seek(SeekFrom::End(-(TEXTURE_CACHE_FOOTER_SIZE as i64)))
            .map_err(|e| format!("failed to seek to footer position: {}", e))?;

        let footer = CacheFooter::read(&mut file).and_then(CacheFooter::verify)?;

        let info_block_size = footer.string_table_size
            + footer.chunk_table_entries * TEXTURE_CACHE_MIPMAP_OFFSET_SIZE
            + footer.item_count * TEXTURE_CACHE_ITEM_ENTRY_SIZE;

        file.seek(SeekFrom::End(
            -((info_block_size + TEXTURE_CACHE_FOOTER_SIZE) as i64),
        ))
        .map_err(|e| format!("failed to seek to item info chunk position: {}", e))?;

        let keyword = filter.unwrap_or("");
        let items = ItemsInfoChunk::read(&footer, &mut file)?
            .drain(..)
            .filter(|i| i.name.contains(keyword))
            .collect();

        Ok(ContainerInfo {
            path: path.to_owned(),
            items,
        })
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ContainerItemInfo for CacheItem {
    // ------------------------------------------------------------------------
    fn name(&self) -> &str {
        &self.name
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> From<(&'a CacheItem, Vec<u8>)> for Texture {
    // ------------------------------------------------------------------------
    fn from((item, data): (&'a CacheItem, Vec<u8>)) -> Self {
        if item.is_cube {
            // TODO check assumptions
            assert!(item.layer_count == 0);
            Self::CubeMap(
                item.format,
                Dimensions {
                    width: item.width,
                    height: item.height,
                },
                MipCount(item.mip_count),
                LayerCount(item.layer_count),
                data,
            )
        } else if item.layer_count > 1 {
            Self::Array(
                item.format,
                Dimensions {
                    width: item.width,
                    height: item.height,
                },
                MipCount(item.mip_count),
                LayerCount(item.layer_count),
                data,
            )
        } else {
            Self::Image(
                item.format,
                Dimensions {
                    width: item.width,
                    height: item.height,
                },
                MipCount(item.mip_count),
                data,
            )
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ItemDataReader<Texture> for ItemCollection<CacheItem> {
    // ------------------------------------------------------------------------
    fn read_data(&self, item: &str) -> Result<Texture, String> {
        if let Some((cache, item)) = self.items.get(item) {
            let cache_path = self.container.get(cache).expect("texture cache path");

            let item_name = || -> String {
                format!(
                    "item {} from texture cache {}",
                    item.name,
                    cache_path.display()
                )
            };

            let mut compressed =
                utils::try_allocate_memory(item.compressed_size as usize, &item.name)?;
            let mut uncompressed =
                utils::try_reserve_buffer(item.uncompressed_size as usize, &item.name)?;

            let mut file = File::open(cache_path)
                .map(BufReader::new)
                .map_err(|e| format!("failed to open {}: {}", cache_path.display(), e))?;

            file.seek_relative(item.page_offset as i64 * 4096)
                .map_err(|e| format!("failed to seek to {}: {}", item_name(), e))?;

            ReadHelper::read_exact(&mut file, &mut compressed, &item_name())?;

            // --- collect chunks
            let mut start_offset = 0;
            let mut chunks = Vec::default();
            for next_offset in &item.chunk_offsets {
                let next_offset = *next_offset as usize;
                chunks.push(&compressed[start_offset..next_offset]);
                start_offset = next_offset;
            }
            if start_offset < compressed.len() {
                chunks.push(&compressed[start_offset..compressed.len()]);
            }

            use flate2::read::ZlibDecoder;

            for (i, chunk) in chunks.iter_mut().enumerate() {
                let _compressed_size = chunk.read_u32("chunksize")?;
                let uncompressed_size = chunk.read_u32("uncompressed chunk size")?;
                let _remaining = chunk.read_u8("remaining chunks")?;

                let mut uncompressed_chunk = utils::try_reserve_buffer(
                    uncompressed_size as usize,
                    &format!("chunk #{} of {}", i, &item.name),
                )?;

                ZlibDecoder::new(chunk)
                    .read_to_end(&mut uncompressed_chunk)
                    .map_err(|e| format!("failed to extract/uncompress {}: {}", item_name(), e))?;

                uncompressed.append(&mut uncompressed_chunk);
            }

            if uncompressed.len() != item.uncompressed_size as usize {
                return Err(format!(
                    "uncompressed data for {} does not matched expected size: {} != {}",
                    item_name(),
                    uncompressed.len(),
                    item.uncompressed_size
                ));
            }
            // TODO
            // checksum/hash check?
            Ok(Texture::from((item, uncompressed)))
        } else {
            Err(format!("item not found in cache: {}", item))
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// texturecache format parser
// ----------------------------------------------------------------------------
use super::utils::{ReadHelper, VerifyHelper};
// ----------------------------------------------------------------------------
const TEXTURE_CACHE_FOOTER_MAGIC: [u8; 4] = [0x48, 0x43, 0x58, 0x54];
// ----------------------------------------------------------------------------
#[derive(Clone, Debug)]
pub struct CacheItem {
    name: String,
    _unknown1: u32, // probably checksum or hash for data, name or item entry
    _namestring_offset: u32,
    page_offset: u32,
    compressed_size: u32,
    uncompressed_size: u32,
    alignment: u32, // always 16 in vanilla

    width: u16,
    height: u16,
    mip_count: u16,
    layer_count: u16, // e.g. texture array level
    chunk_offsets: Vec<u32>,
    _unknown2: u64,
    format: TextureFormat,
    is_cube: bool,
    _unknown3: u8,
}
// ----------------------------------------------------------------------------
#[derive(Debug)]
struct CacheFooter {
    _checksum: u64,
    _pages: u32, // pages * 4096b
    item_count: u32,
    string_table_size: u32,
    chunk_table_entries: u32,
    magic: [u8; 4],
    version: u32,
}
// ----------------------------------------------------------------------------
impl CacheFooter {
    // ------------------------------------------------------------------------
    fn read(r: &mut impl ReadHelper) -> Result<Self, String> {
        let mut magic = [0; 4];

        Ok(Self {
            _checksum: r.read_u64("checksum")?,
            _pages: r.read_u32("used pages")?,
            item_count: r.read_u32("items")?,
            string_table_size: r.read_u32("stringtable size")?,
            chunk_table_entries: r.read_u32("chunk table entries")?,
            magic: r.read_exact(&mut magic, "footer magic").map(|_| magic)?,
            version: r.read_u32("version")?,
        })
    }
    // ------------------------------------------------------------------------
    fn verify(self) -> Result<Self, String> {
        if self.magic != TEXTURE_CACHE_FOOTER_MAGIC {
            return Err(format!(
                "texture cache file footer magic mismatch. expected {:?} found: {:?}",
                &TEXTURE_CACHE_FOOTER_MAGIC, &self.magic
            ));
        }
        // TODO checksum? of complete file or only footer?
        Self::verify_known(self.version, 0x06, "version")?;

        Ok(self)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
struct ItemsInfoChunk(Vec<CacheItem>);
// ----------------------------------------------------------------------------
impl std::ops::Deref for ItemsInfoChunk {
    type Target = Vec<CacheItem>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
// ----------------------------------------------------------------------------
impl std::ops::DerefMut for ItemsInfoChunk {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
// ----------------------------------------------------------------------------
impl ItemsInfoChunk {
    // ------------------------------------------------------------------------
    fn read(info: &CacheFooter, stream: &mut impl ReadHelper) -> Result<Self, String> {
        // -- chunk entries table
        let chunk_offsets = (0..info.chunk_table_entries)
            .map(|i| stream.read_u32(&format!("chunk offset #{}", i)))
            .collect::<Result<Vec<_>, String>>()
            .map_err(|e| format!("failed to read chunk table: {}", e))?;

        // -- string table
        let item_names = (0..info.item_count)
            .map(|i| stream.read_zero_terminated_string(255, &format!("string #{}", i)))
            .collect::<Result<Vec<_>, String>>()
            .map_err(|e| format!("failed to read string table: {}", e))?;

        // assumption is: item data is ordered as string table
        // TODO: check name string offsets in item info?
        // -- item entries table
        let items = item_names
            .iter()
            .map(|name| {
                CacheItem::read(name, stream, &chunk_offsets)
                    .map_err(|e| format!("failed to read cache item info for [{}]: {}", name, e))
                    .and_then(CacheItem::verify)
            })
            .collect::<Result<_, String>>()?;

        Ok(Self(items))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl CacheItem {
    // ------------------------------------------------------------------------
    fn read(name: &str, r: &mut impl ReadHelper, chunk_offsets: &[u32]) -> Result<Self, String> {
        Ok(Self {
            name: name.to_string(),
            _unknown1: r.read_u32("item hash/crc?")?,
            _namestring_offset: r.read_u32("namestring offset")?,
            page_offset: r.read_u32("page_offset")?,
            compressed_size: r.read_u32("compressed site")?,
            uncompressed_size: r.read_u32("uncompressed_size")?,
            alignment: r.read_u32("alignment ?")?,
            width: r.read_u16("width")?,
            height: r.read_u16("height")?,
            mip_count: r.read_u16("texture mip count")?,
            layer_count: r.read_u16("texture layer count")?,
            chunk_offsets: {
                let offset_index = r.read_u32("chunk offset index")?;
                let offset_count = r.read_u32("chunk offset num")?;
                (0..offset_count)
                    .map(|i| {
                        chunk_offsets
                            .get((offset_index + i as u32) as usize)
                            .copied()
                            .ok_or_else(|| "found invalid chunk index access".to_string())
                    })
                    .collect::<Result<Vec<_>, String>>()?
            },
            _unknown2: r.read_u64("unknown1")?,
            format: TextureFormat::try_from((
                r.read_u8("TextureType 1")?,
                r.read_u8("TextureType 2")?,
            ))?,
            is_cube: r.read_u8("is_cube")? == 1,
            _unknown3: r.read_u8("unknown")?,
        })
    }
    // ------------------------------------------------------------------------
    fn verify(self) -> Result<Self, String> {
        if self.name.is_empty() {
            return Err("found cache item entry without a name.".to_string())
        }

        Self::verify_known(self.alignment, 16, "alignment size")?;

        Ok(self)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TryFrom<(u8, u8)> for TextureFormat {
    type Error = String;

    fn try_from((compression, unknown): (u8, u8)) -> Result<Self, Self::Error> {
        match (compression, unknown) {
            (0x07, 0x04) => Ok(Self::Bc1Unorm),
            (0x08, 0x04) => Ok(Self::Bc3Unorm),
            (0x00, 0x00)
            | (0xfd, 0x03)
            | (0x0A, 0x04)
            | (0x0D, 0x04)
            | (0x0E, 0x04)
            | (0x0F, 0x04) => Ok(Self::Unsupported(compression, unknown)),
            _ => Err(format!(
                "unknown compression/type info: ({}, {})",
                compression, unknown
            )),
        }
    }
}
// ----------------------------------------------------------------------------
// helper
// ----------------------------------------------------------------------------
impl VerifyHelper for CacheFooter {}
impl VerifyHelper for CacheItem {}
// ----------------------------------------------------------------------------

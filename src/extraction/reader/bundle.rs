// ----------------------------------------------------------------------------
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::Path;

use crate::utils;

use super::doboz;
use flate2::Crc;

use super::{
    Container, ContainerInfo, ContainerItemInfo, ItemCollection, ItemDataReader, Registry,
};
// ----------------------------------------------------------------------------
/// NewType wrapper to reduce exposed traits and internal datatypes (e.g. BundleItem)
#[derive(Default)]
pub struct BundleRegistry(Registry<BundleItem>);
// ----------------------------------------------------------------------------
impl BundleRegistry {
    // ------------------------------------------------------------------------
    pub fn init(&mut self, game_path: &Path, filter: Option<&str>) -> Result<(), String> {
        self.0.init(game_path, filter)
    }
    // ------------------------------------------------------------------------
    pub fn filter<F: Fn(&str) -> bool>(&self, filter: F) -> ItemCollection<BundleItem> {
        self.0.filter(filter)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Container for BundleItem {
    type Item = BundleItem;
    // ------------------------------------------------------------------------
    fn file_extension() -> &'static str {
        ".bundle"
    }
    // ------------------------------------------------------------------------
    fn read_info(path: &Path, filter: Option<&str>) -> Result<ContainerInfo<Self::Item>, String> {
        let mut file = File::open(path)
            .map(BufReader::new)
            .map_err(|e| format!("failed to open {}: {}", path.display(), e))?;

        let header = BundleHeader::read(&mut file).and_then(BundleHeader::verify)?;

        let keyword = filter.unwrap_or("");
        let items = ItemsInfoChunk::read_max(header.item_info_size, &mut file)?
            .drain(..)
            .filter(|i| i.name.contains(keyword))
            .collect();

        Ok(ContainerInfo {
            path: path.to_owned(),
            items,
        })
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ContainerItemInfo for BundleItem {
    // ------------------------------------------------------------------------
    fn name(&self) -> &str {
        &self.name
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ItemDataReader<Vec<u8>> for ItemCollection<BundleItem> {
    // ------------------------------------------------------------------------
    fn read_data(&self, item: &str) -> Result<Vec<u8>, String> {
        if let Some((bundle, item)) = self.items.get(item) {
            let bundle_path = self.container.get(bundle).expect("bundle path");

            let item_name = || -> String {
                format!("item {} from bundle {}", item.name, bundle_path.display())
            };

            let mut compressed =
                utils::try_allocate_memory(item.compressed_size as usize, &item.name)?;
            let mut uncompressed =
                utils::try_reserve_buffer(item.uncompressed_size as usize, &item.name)?;

            let mut file = File::open(bundle_path)
                .map(BufReader::new)
                .map_err(|e| format!("failed to open {}: {}", bundle_path.display(), e))?;

            file.seek_relative(item.offset as i64)
                .map_err(|e| format!("failed to seek to {}: {}", item_name(), e))?;

            ReadHelper::read_exact(&mut file, &mut compressed, &item_name())?;

            let uncompressed = match item.compression {
                BundleCompression::Uncompressed => Ok(compressed),
                BundleCompression::Zlib => {
                    use flate2::read::ZlibDecoder;

                    ZlibDecoder::new(compressed.as_slice())
                        .read_to_end(&mut uncompressed)
                        .map_err(|e| format!("zlib uncompress error: {}", e))
                        .map(|_| uncompressed)
                }
                BundleCompression::Snappy => {
                    use snap::read::FrameDecoder as SnappyDecoder;

                    SnappyDecoder::new(compressed.as_slice())
                        .read_to_end(&mut uncompressed)
                        .map_err(|e| format!("snappy uncompress error: {}", e))
                        .map(|_| uncompressed)
                }
                BundleCompression::Doboz => {
                    use doboz::DobozDecoder;

                    DobozDecoder::new(compressed.as_slice())
                        .map_err(|e| format!("doboz uncompress error: {}", e))?
                        .read_to_end(&mut uncompressed)
                        .map_err(|e| format!("doboz uncompress error: {}", e))
                        .map(|_| uncompressed)
                }
                BundleCompression::Lz4 => {
                    use lz_fear::raw::decompress_raw as lz4_decompress;

                    lz4_decompress(
                        &compressed,
                        &compressed,
                        &mut uncompressed,
                        item.uncompressed_size as usize,
                    )
                    .map_err(|e| format!("lz4 uncompress error: {}", e))
                    .map(|_| uncompressed)
                }
            }
            .map_err(|e: String| format!("failed to extract/uncompress {}: {}", item_name(), e))?;

            // crc32 check
            // Note: patch1 items do not have a checksum stored
            if item.checksum == 0 {
                Ok(uncompressed)
            } else {
                let mut crc = Crc::new();
                crc.update(&uncompressed);
                if item.checksum != crc.sum() {
                    Err(format!("checksum check failed for {}", item_name()))
                } else {
                    Ok(uncompressed)
                }
            }
        } else {
            Err(format!("item not found in bundle: {}", item))
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// bundle format parser
// ----------------------------------------------------------------------------
use super::utils::{ReadHelper, VerifyHelper};
// ----------------------------------------------------------------------------
const BUNDLE_HEADER_MAGIC: [u8; 8] = [0x50, 0x4F, 0x54, 0x41, 0x54, 0x4F, 0x37, 0x30];

#[allow(dead_code)]
struct BundleHeader {
    magic: [u8; 8],
    filesize: u32,
    unknown1: u32,
    item_info_size: u32,
    unknown2: u32, // always 03 00 01 00
    unknown3: u64, // always 00 13 13 13 13 13 13 13
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, Copy)]
enum BundleCompression {
    Uncompressed,
    Zlib,
    Snappy,
    Doboz,
    Lz4,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub struct BundleItem {
    name: String,
    unknown1: [u8; 16], // 16 bytes
    unknown2: u32,      // always zero
    uncompressed_size: u32,
    compressed_size: u32,
    offset: u32,
    datetime: u64,
    unknown3: [u8; 16], // always zero
    checksum: u32,      // crc32 of uncompressed data
    compression: BundleCompression,
}
// ----------------------------------------------------------------------------
impl BundleHeader {
    // ------------------------------------------------------------------------
    fn read(r: &mut impl ReadHelper) -> Result<Self, String> {
        let mut magic = [0; 8];
        r.read_exact(&mut magic, "header magic")?;

        Ok(Self {
            magic,
            filesize: r.read_u32("filesize")?,
            unknown1: r.read_u32("unknown 1")?,
            item_info_size: r.read_u32("iteminfo chunk size")?,
            unknown2: r.read_u32("unknown 2")?,
            unknown3: r.read_u64("unknown 3")?,
        })
    }
    // ------------------------------------------------------------------------
    fn verify(self) -> Result<Self, String> {
        if self.magic != BUNDLE_HEADER_MAGIC {
            return Err(format!(
                "bundle file header magic mismatch. expected [POTATO70] found: {}",
                String::from_utf8_lossy(&self.magic)
            ));
        }
        Self::verify_unknown(self.unknown2, 0x0001_0003, "unknown 2")?;
        Self::verify_unknown(self.unknown3, 0x1313_1313_1313_1300, "unknown 3")?;

        Ok(self)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
struct ItemsInfoChunk(Vec<BundleItem>);
// ----------------------------------------------------------------------------
impl std::ops::Deref for ItemsInfoChunk {
    type Target = Vec<BundleItem>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
// ----------------------------------------------------------------------------
impl std::ops::DerefMut for ItemsInfoChunk {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
// ----------------------------------------------------------------------------
impl ItemsInfoChunk {
    // ------------------------------------------------------------------------
    fn read_max(max: u32, stream: &mut impl ReadHelper) -> Result<Self, String> {
        // Note: every iteminfo is 320 bytes
        let max_count = (max / 320) as usize;
        let mut items = Vec::with_capacity(max_count);

        for _ in 0..max_count {
            if let Some(item) = BundleItem::read(stream).and_then(BundleItem::verify)? {
                items.push(item);
            }
        }

        Ok(Self(items))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl BundleItem {
    // ------------------------------------------------------------------------
    fn read(r: &mut impl ReadHelper) -> Result<Self, String> {
        fn read_unknown(r: &mut impl ReadHelper, name: &str) -> Result<[u8; 16], String> {
            let mut data = [0; 16];
            r.read_exact(&mut data, name)?;
            Ok(data)
        }

        Ok(Self {
            name: r.read_fixed_bufsize_zero_terminated_string(256, "bundle item path")?,
            unknown1: read_unknown(r, "unknown 1")?,
            unknown2: r.read_u32("unknwon 2")?,
            uncompressed_size: r.read_u32("uncompressed size")?,
            compressed_size: r.read_u32("compressed size")?,
            offset: r.read_u32("offset")?,
            datetime: r.read_u64("date time")?,
            unknown3: read_unknown(r, "unknown 3")?,
            checksum: r.read_u32("bundle item data checksum")?,
            compression: BundleCompression::try_from(r.read_u32("bundle item data compression")?)?,
        })
    }
    // ------------------------------------------------------------------------
    fn verify(self) -> Result<Option<Self>, String> {
        if self.name.is_empty() {
            if self.unknown1 != [0; 16] {
                return Err(format!(
                    "expected empty unknown 1. found {:?}",
                    self.unknown1
                ));
            }
            Self::verify_known(self.unknown2, 0, "empty unknown 2")?;
            Self::verify_known(self.uncompressed_size, 0, "empty uncompressed size")?;
            Self::verify_known(self.compressed_size, 0, "empty compressed size")?;
            Self::verify_known(self.offset, 0, "empty offset")?;
            Self::verify_known(self.datetime, 0, "empty datetime")?;
            if self.unknown3 != [0; 16] {
                return Err(format!(
                    "expected empty unknown 3. found {:?}",
                    self.unknown3
                ));
            }
            Self::verify_known(self.checksum, 0, "empty checksum")?;
            if matches!(self.compression, BundleCompression::Uncompressed) {
                return Err(format!(
                    "expected empty compression type. found {:?}",
                    self.compression
                ));
            }
            Ok(None)
        } else {
            Self::verify_known(self.unknown2, 0, "unknown 2")?;
            if self.unknown3 != [0; 16] {
                return Err(format!(
                    "expected empty unknown 3. found {:?}",
                    self.unknown3
                ));
            }
            Ok(Some(self))
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl TryFrom<u32> for BundleCompression {
    type Error = String;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Self::Uncompressed),
            1 => Ok(Self::Zlib),
            2 => Ok(Self::Snappy),
            3 => Ok(Self::Doboz),
            4 | 5 => Ok(Self::Lz4),
            _ => Err(format!("unsupported compression type {}", value)),
        }
    }
}
// ----------------------------------------------------------------------------
// helper
// ----------------------------------------------------------------------------
impl VerifyHelper for BundleHeader {}
impl VerifyHelper for BundleItem {}
// ----------------------------------------------------------------------------
impl std::fmt::Debug for BundleItem {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}
// ----------------------------------------------------------------------------

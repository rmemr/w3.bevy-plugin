// ----------------------------------------------------------------------------
// Doboz Decompressor based on: https://github.com/nemequ/doboz
//
// this is a port to support decompression and not a 1:1 port including all
// optimizations from the original. some workarounds to make it easier.
// check out the original source!
// ----------------------------------------------------------------------------
/*
 * Doboz Data Compression Library
 * Copyright (C) 2010-2011 Attila T. Afra <attila.afra@gmail.com>
 *
 * This software is provided 'as-is', without any express or implied warranty. In no event will
 * the authors be held liable for any damages arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose, including commercial
 * applications, and to alter it and redistribute it freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not claim that you wrote the
 *    original software. If you use this software in a product, an acknowledgment in the product
 *    documentation would be appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be misrepresented as
 *    being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */
// ----------------------------------------------------------------------------
use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
// ----------------------------------------------------------------------------
const VERSION: u8 = 0; // encoding format
// ----------------------------------------------------------------------------
pub type Result<T> = std::result::Result<T, DobozError>;
// ----------------------------------------------------------------------------
#[derive(Debug)]
pub enum DobozError {
    BufferReadError(String),
    BufferWriteError(String),
    BufferAllocationFailed(String),
    BufferCopyFailed(String),

    CorruptedData(String),
    UnsupportedVersion(String),
}
// ----------------------------------------------------------------------------
pub struct DobozDecoder<R: ReadBytesExt> {
    reader: Reader<R>,
    header: Header,
}
// ----------------------------------------------------------------------------
struct Header {
    uncompressed_size: usize,
    _compressed_size: usize,
    version: u8,
    is_stored: bool,
}
// ----------------------------------------------------------------------------
// Note: in this implementation WORD_SIZE *must* be 4 bytes as write/read_u32 is hardcoded
const WORD_SIZE: usize = 4;
const MIN_MATCH_LENGTH: usize = 3;

// prevents fast write operations from writing beyond the end of the buffer during decoding
const TAIL_LENGTH: usize = 2 * WORD_SIZE;

const RUN_LENGTH: [u8; 8] = [4, 1, 2, 1, 3, 1, 2, 1];

// ----------------------------------------------------------------------------
impl<R: ReadBytesExt> DobozDecoder<R> {
    // ------------------------------------------------------------------------
    pub fn new(reader: R) -> Result<Self> {
        let mut reader = Reader::new(reader);

        let header = Header::decode(&mut reader).and_then(|header| header.verify(VERSION))?;

        Ok(Self { reader, header })
    }
    // ------------------------------------------------------------------------
    // TODO this should just be provided by an io::Read implementation
    pub fn read_to_end(&mut self, buf: &mut Vec<u8>) -> Result<usize> {
        assert!(buf.is_empty(), "provided target buffer must be empty");

        buf.try_reserve_exact(self.header.uncompressed_size)
            .map_err(|e| {
                DobozError::BufferAllocationFailed(format!(
                    "target buffer size {}: {}",
                    self.header.uncompressed_size, e
                ))
            })?;

        if self.header.is_stored {
            return self.reader.read_to_end(buf);
        }

        let output_end = self.header.uncompressed_size;

        // compute pointer to the first byte of the output 'tail'
        // fast write operations can be used only before the tail, because those
        // may write beyond the end of the output buffer
        let output_tail = if self.header.uncompressed_size > TAIL_LENGTH {
            output_end - TAIL_LENGTH
        } else {
            0
        };

        // initialize the control word to 'empty'
        let mut control_word = 1;

        let mut buf = Writer(buf);

        // decoding loop
        loop {
            // check whether there is enough data left in the input buffer
            // in order to decode the next literal/match, we have to read up to 8 bytes (2 words)
            // thanks to the trailing dummy, there must be at least 8 remaining input bytes

            // check whether we must read a control word
            if control_word == 1 {
                control_word = self.reader.read_u32_named("controlword")?;
            }

            // detect whether it's a literal or a match
            if control_word & 1 == 0 {
                // it's a literal

                // if we are before the tail, we can safely use fast writing operations
                if buf.len() < output_tail {
                    let idx = ((control_word >> 1) as usize) & 0b0111;
                    let run_length: u8 = RUN_LENGTH[idx];
                    match run_length {
                        1 => buf.write_u8(self.reader.read_u8()?)?,
                        2 => buf.write_u16(self.reader.read_u16()?)?,
                        3 => buf.write_u24(self.reader.read_u24()?)?,
                        4 => buf.write_u32(self.reader.read_u32()?)?,
                        _ => unreachable!(),
                    }
                    // consume as much control word bits as the run length
                    control_word >>= run_length;
                } else {
                    // we have reached the tail, we cannot output literals in runs anymore
                    // output all remaining literals
                    while buf.len() < output_end {
                        // check whether we must read a control word
                        if control_word == 1 {
                            control_word = self.reader.read_u32_named("controlword")?;
                        }

                        // output one literal
                        buf.write_u8(self.reader.read_u8()?)?;

                        // next control word bit
                        control_word >>= 1;
                    }

                    // done
                    return Ok(self.reader.pos());
                }
            } else {
                // it's a match

                // decode the match
                let datamatch = Match::decode(&mut self.reader)?;

                // Note: buf will grow with the writes, so it is valid if
                // datamatch.offset + datamatch.length points beyond buf size
                // at *start* of match handling
                // Workaround in this version:
                //  unfortunately match_start can be negative -> i64
                let mut match_start = (buf.len() - datamatch.offset) as i64;
                let mut i = 0;

                if datamatch.offset < WORD_SIZE {
                    // the match offset is less than the word size
                    // in order to correctly handle the overlap, we have to copy
                    // the first three bytes one by one

                    // therefore this copy has to be bytewise
                    buf.write_u8(buf.0[(match_start) as usize])?;
                    buf.write_u8(buf.0[(match_start + 1) as usize])?;
                    buf.write_u8(buf.0[(match_start + 2) as usize])?;

                    i += 3;

                    // with this trick, we increase the distance between the source
                    // and destination pointers
                    // this enables us to use fast copying for the rest of the match

                    match_start -= (2 + (datamatch.offset & 1)) as i64;
                }

                // fast copying
                // there must be no overlap between the source and destination words

                // Workaround in this version:
                //  write_u32 appends always 4 bytes but often (~10:1) this
                //  overshoots and the buf needs to be truncated again
                //
                if (datamatch.length - i) % 4 != 0 {
                    while i < datamatch.length {
                        let value = buf.read_u32((match_start + i as i64) as usize)?;
                        buf.write_u32(value)?;
                        i += WORD_SIZE;
                    }

                    buf.0.truncate(buf.len() - i + datamatch.length);
                } else {
                    while i < datamatch.length {
                        let value = buf.read_u32((match_start + i as i64) as usize)?;
                        buf.write_u32(value)?;
                        i += WORD_SIZE;
                    }
                }

                control_word >>= 1;
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
#[derive(Debug)]
struct Match {
    length: usize,
    offset: usize,
}
// ----------------------------------------------------------------------------
struct MatchDecoding {
    mask: u32,
    offset_shift: u8,
    length_mask: u8,
    length_shift: u8,
    size: u8,
}
// ----------------------------------------------------------------------------
impl MatchDecoding {
    const fn new(mask: u32, offset_shift: u8, length_mask: u8, length_shift: u8, size: u8) -> Self {
        Self {
            mask,
            offset_shift,
            length_mask,
            length_shift,
            size,
        }
    }
}
// ----------------------------------------------------------------------------
#[rustfmt::skip]
const MATCH_LUT: [MatchDecoding;8] = [
    MatchDecoding::new(0x00ff     ,  2,   0, 0, 1), // (0)00
    MatchDecoding::new(0xffff     ,  2,   0, 0, 2), // (0)01
    MatchDecoding::new(0xffff     ,  6,  15, 2, 2), // (0)10
    MatchDecoding::new(0x00ff_ffff,  8,  31, 3, 3), // (0)11
    MatchDecoding::new(0x00ff     ,  2,   0, 0, 1), // (1)00 = (0)00
    MatchDecoding::new(0xffff     ,  2,   0, 0, 2), // (1)01 = (0)01
    MatchDecoding::new(0xffff     ,  6,  15, 2, 2), // (1)10 = (0)10
    MatchDecoding::new(0xffff_ffff, 11, 255, 3, 4), // 111
];
// ----------------------------------------------------------------------------
impl Match {
    // ------------------------------------------------------------------------
    #[inline(always)]
    fn decode(stream: &mut impl StreamReader) -> Result<Self> {
        let b = stream.read_u8_named("match control byte")?;
        let decoding = &MATCH_LUT[(b & 7) as usize];

        let data_word = match decoding.size {
            1 => b as u32,
            2 => b as u32 | ((stream.read_u8()? as u32) << 8),
            3 => b as u32 | ((stream.read_u16()? as u32) << 8),
            4 => b as u32 | ((stream.read_u24()? as u32) << 8),
            _ => unreachable!(),
        };
        Ok(Match {
            offset: ((data_word & decoding.mask) >> decoding.offset_shift) as usize,
            length: ((data_word >> decoding.length_shift) & decoding.length_mask as u32) as usize
                + MIN_MATCH_LENGTH,
        })
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl Header {
    // ------------------------------------------------------------------------
    fn decode(stream: &mut impl StreamReader) -> Result<Self> {
        let attributes = stream.read_u8_named("header attributes")?;
        let size_codesize = ((attributes >> 3) & 7) + 1;

        let (uncompressed_size, _compressed_size) = match size_codesize {
            1 => (
                stream.read_u8_named("header uncompressed size")? as usize,
                stream.read_u8_named("header compressed size")? as usize,
            ),
            2 => (
                stream.read_u16_named("header uncompressed size")? as usize,
                stream.read_u16_named("header compressed size")? as usize,
            ),
            4 => (
                stream.read_u32_named("header uncompressed size")? as usize,
                stream.read_u32_named("header compressed size")? as usize,
            ),
            8 => (
                stream.read_u64_named("header uncompressed size")? as usize,
                stream.read_u64_named("header compressed size")? as usize,
            ),
            _ => {
                return Err(DobozError::CorruptedData(String::from(
                    "invalid header sizecode data",
                )))
            }
        };

        Ok(Self {
            uncompressed_size,
            _compressed_size,
            version: attributes & 7,
            is_stored: (attributes & 128) != 0,
        })
    }
    // ------------------------------------------------------------------------
    fn verify(self, version: u8) -> Result<Self> {
        if self.version != version {
            Err(DobozError::UnsupportedVersion(format!(
                "expected {}. found in header: {}",
                version, self.version
            )))
        } else {
            Ok(self)
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// helper
// ----------------------------------------------------------------------------
trait StreamReader {
    // ------------------------------------------------------------------------
    fn read_u8(&mut self) -> Result<u8>;
    fn read_u16(&mut self) -> Result<u16>;
    fn read_u24(&mut self) -> Result<u32>;
    fn read_u32(&mut self) -> Result<u32>;
    fn read_u64(&mut self) -> Result<u64>;
    // ------------------------------------------------------------------------
    #[inline(always)]
    fn read_u8_named(&mut self, name: &str) -> Result<u8> {
        self.read_u8()
            .map_err(|e| DobozError::BufferReadError(format!("[{}] element: {}", e.msg(), name)))
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    fn read_u16_named(&mut self, name: &str) -> Result<u16> {
        self.read_u16()
            .map_err(|e| DobozError::BufferReadError(format!("[{}] element: {}", e.msg(), name)))
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    fn read_u32_named(&mut self, name: &str) -> Result<u32> {
        self.read_u32()
            .map_err(|e| DobozError::BufferReadError(format!("[{}] element: {}", e.msg(), name)))
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    fn read_u64_named(&mut self, name: &str) -> Result<u64> {
        self.read_u64()
            .map_err(|e| DobozError::BufferReadError(format!("[{}] element: {}", e.msg(), name)))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
trait StreamWriter {
    // ------------------------------------------------------------------------
    fn write_u8(&mut self, v: u8) -> Result<()>;
    fn write_u16(&mut self, v: u16) -> Result<()>;
    fn write_u24(&mut self, v: u32) -> Result<()>;
    fn write_u32(&mut self, v: u32) -> Result<()>;
    fn write_u64(&mut self, v: u64) -> Result<()>;
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
struct Reader<R: ReadBytesExt> {
    rdr: R,
    pos: usize,
}
// ----------------------------------------------------------------------------
struct Writer<'buf>(&'buf mut Vec<u8>);
// ----------------------------------------------------------------------------
impl<R: ReadBytesExt> Reader<R> {
    // ------------------------------------------------------------------------
    fn new(rdr: R) -> Self {
        Self { rdr, pos: 0 }
    }
    // ------------------------------------------------------------------------
    fn pos(&self) -> usize {
        self.pos
    }
    // ------------------------------------------------------------------------
    fn read_to_end(&mut self, buf: &mut Vec<u8>) -> Result<usize> {
        self.rdr
            .read_to_end(buf)
            .map_err(|e| DobozError::BufferCopyFailed(format!("{}", e)))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<R: ReadBytesExt> StreamReader for Reader<R> {
    // ------------------------------------------------------------------------
    fn read_u8(&mut self) -> Result<u8> {
        self.pos += 1;
        self.rdr
            .read_u8()
            .map_err(|e| DobozError::BufferReadError(format!("pos {}: {}", self.pos - 1, e)))
    }
    // ------------------------------------------------------------------------
    fn read_u16(&mut self) -> Result<u16> {
        self.pos += 2;
        self.rdr
            .read_u16::<LittleEndian>()
            .map_err(|e| DobozError::BufferReadError(format!("pos {}: {}", self.pos - 2, e)))
    }
    // ------------------------------------------------------------------------
    fn read_u24(&mut self) -> Result<u32> {
        self.pos += 3;
        self.rdr
            .read_u24::<LittleEndian>()
            .map_err(|e| DobozError::BufferReadError(format!("pos {}: {}", self.pos - 3, e)))
    }
    // ------------------------------------------------------------------------
    fn read_u32(&mut self) -> Result<u32> {
        self.pos += 4;
        self.rdr
            .read_u32::<LittleEndian>()
            .map_err(|e| DobozError::BufferReadError(format!("pos {}: {}", self.pos - 4, e)))
    }
    // ------------------------------------------------------------------------
    fn read_u64(&mut self) -> Result<u64> {
        self.pos += 8;
        self.rdr
            .read_u64::<LittleEndian>()
            .map_err(|e| DobozError::BufferReadError(format!("pos {}: {}", self.pos - 8, e)))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'buf> Writer<'buf> {
    // ------------------------------------------------------------------------
    fn len(&self) -> usize {
        self.0.len()
    }
    // ------------------------------------------------------------------------
    #[inline(always)]
    fn read_u32(&mut self, pos: usize) -> Result<u32> {
        (&self.0[pos..pos + 4])
            .read_u32::<LittleEndian>()
            .map_err(|e| DobozError::BufferReadError(format!("target buffer pos {}: {}", pos, e)))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'buf> StreamWriter for Writer<'buf> {
    // ------------------------------------------------------------------------
    fn write_u8(&mut self, v: u8) -> Result<()> {
        WriteBytesExt::write_u8(self.0, v)
            .map_err(|e| DobozError::BufferWriteError(format!("{}", e)))
    }
    // ------------------------------------------------------------------------
    fn write_u16(&mut self, v: u16) -> Result<()> {
        WriteBytesExt::write_u16::<LittleEndian>(self.0, v)
            .map_err(|e| DobozError::BufferWriteError(format!("{}", e)))
    }
    // ------------------------------------------------------------------------
    fn write_u24(&mut self, v: u32) -> Result<()> {
        WriteBytesExt::write_u24::<LittleEndian>(self.0, v)
            .map_err(|e| DobozError::BufferWriteError(format!("{}", e)))
    }
    // ------------------------------------------------------------------------
    fn write_u32(&mut self, v: u32) -> Result<()> {
        WriteBytesExt::write_u32::<LittleEndian>(self.0, v)
            .map_err(|e| DobozError::BufferWriteError(format!("{}", e)))
    }
    // ------------------------------------------------------------------------
    fn write_u64(&mut self, v: u64) -> Result<()> {
        WriteBytesExt::write_u64::<LittleEndian>(self.0, v)
            .map_err(|e| DobozError::BufferWriteError(format!("{}", e)))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DobozError {
    fn msg(self) -> String {
        match self {
            DobozError::BufferReadError(msg) => msg,
            DobozError::BufferAllocationFailed(msg) => msg,
            DobozError::BufferCopyFailed(msg) => msg,
            DobozError::BufferWriteError(msg) => msg,

            DobozError::CorruptedData(msg) => msg,
            DobozError::UnsupportedVersion(msg) => msg,
        }
    }
}
// ----------------------------------------------------------------------------
impl std::fmt::Display for DobozError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            DobozError::BufferReadError(info) => write!(f, "BufferReadError: {}", info),
            DobozError::BufferWriteError(info) => write!(f, "BufferWriteError: {}", info),
            DobozError::BufferAllocationFailed(info) => write!(f, "BufferAllocationFailed: {}", info),
            DobozError::BufferCopyFailed(info) => write!(f, "BufferCopyFailed: {}", info),
            DobozError::CorruptedData(info) => write!(f, "CorruptedData: {}", info),
            DobozError::UnsupportedVersion(info) => write!(f, "UnsupportedVersion: {}", info),
        }
    }
}
// ----------------------------------------------------------------------------

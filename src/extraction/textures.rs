// ----------------------------------------------------------------------------
use std::path::Path;

use bevy::prelude::*;
use bevy::tasks::AsyncComputeTaskPool;

use image::{codecs::dds::DdsDecoder, ImageDecoder};

use crate::utils;

use super::reader::{ItemDataReader, TextureCacheRegistry, TextureFormat};
use super::{DataType, ExtractTexturesRequest, TextureExtractionTask};
// ----------------------------------------------------------------------------
pub(super) use super::reader::Texture as TextureData;
// ----------------------------------------------------------------------------
#[derive(Clone, Copy, Debug)]
pub enum TextureType {
    Diffuse,
    Normal,
}
// ----------------------------------------------------------------------------
pub struct ExtractTextureData(pub String);
// ----------------------------------------------------------------------------
impl ExtractTexturesRequest for ExtractTextureData {
    // ------------------------------------------------------------------------
    fn generate_tasks(&self, caches: Res<TextureCacheRegistry>) -> Vec<TextureExtractionTask> {
        let texturepath = self.0.to_string();

        let cache = caches.filter(|name| name == texturepath);

        let task = AsyncComputeTaskPool::get().spawn(async move {
            cache
                .read_data(&texturepath)
                .map(DataType::TextureData)
                .into()
        });

        vec![TextureExtractionTask(task)]
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
pub(super) fn store_dds_as_png(
    filepath: &Path,
    dds: ddsfile::Dds,
    texture_type: TextureType,
) -> Result<(), String> {
    use image::{DynamicImage::*, ImageBuffer};

    // DDS -> png
    let mut buf = Vec::default();
    dds.write(&mut buf)
        .map_err(|e| format!("error storing dds in buf for decoding: {}", e))?;

    let dds_decoder =
        DdsDecoder::new(buf.as_slice()).map_err(|e| format!("could not decode dds: {}", e))?;

    let pixel_size = match texture_type {
        TextureType::Diffuse => 3,
        TextureType::Normal => 4,
    };
    // RGBA
    let mut decoded = utils::try_allocate_memory(
        (dds.get_width() * dds.get_height() * pixel_size) as usize,
        "buffer for decoded dds",
    )?;
    dds_decoder
        .read_image(&mut decoded)
        .map_err(|e| format!("could not read decoded dds image: {}", e))?;

    let rgba_image = match texture_type {
        TextureType::Diffuse => ImageRgb8(
            ImageBuffer::from_raw(dds.get_width(), dds.get_height(), decoded)
                .ok_or_else(|| "failed to create rgb image buffer".to_string())?,
        )
        .into_rgba8(),
        TextureType::Normal => ImageRgba8(
            ImageBuffer::from_raw(dds.get_width(), dds.get_height(), decoded)
                .ok_or_else(|| "failed to create rgba image buffer".to_string())?,
        )
        .into_rgba8(),
    };

    utils::store_image(
        filepath,
        &rgba_image.into_raw(),
        dds.get_width(),
        dds.get_height(),
        png::ColorType::Rgba,
        png::BitDepth::Eight,
        png::Compression::Default,
        None,
    )
}
// ----------------------------------------------------------------------------
// converter
// ----------------------------------------------------------------------------
impl From<TextureFormat> for ddsfile::DxgiFormat {
    fn from(f: TextureFormat) -> Self {
        use TextureFormat::*;
        match f {
            Bc1Unorm => ddsfile::DxgiFormat::BC1_UNorm,
            Bc3Unorm => ddsfile::DxgiFormat::BC3_UNorm,
            Unsupported(_, _) => panic!("Unsupported format!"),
        }
    }
}
// ----------------------------------------------------------------------------
// captions for progress msg
// ----------------------------------------------------------------------------
impl std::fmt::Display for TextureType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            TextureType::Diffuse => write!(f, "diffuse"),
            TextureType::Normal => write!(f, "normal"),
        }
    }
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
use std::path::PathBuf;

use bevy::ecs::schedule::StateData;
use bevy::prelude::*;
// ----------------------------------------------------------------------------
pub struct Witcher3Plugin;
// ----------------------------------------------------------------------------
#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub enum Witcher3HubId {
    Prologue,
    PrologueWinter,
    Skellige,
    Novigrad,
    Velen,
    KaerMorhen,
    Vizima,
    IsleOfMist,
    SpiralDarkValley,
    SpiralDesert,
    SpiralElvenCity,
    SpiralSnowValley,
    Bob,
}
// ----------------------------------------------------------------------------
// resource
pub struct Witcher3Config {
    game_dir: PathBuf,
    terrain: terrain::TerrainConfig,
}
// ----------------------------------------------------------------------------
pub use extraction::{ExtractionProgress, ExtractionRequest, ProgressTrackingState};

pub use terrain::{HubSettings, MaterialSet, TerrainConfig, TerrainMaterialParam};
pub mod environment;
// ----------------------------------------------------------------------------
impl Witcher3Plugin {
    // ------------------------------------------------------------------------
    pub fn extract_data<T: StateData>(state: T) -> SystemSet {
        extraction::Witcher3ExtractPlugin::extract_data(state)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
mod extraction;
mod terrain;
mod utils;
// ----------------------------------------------------------------------------
impl Plugin for Witcher3Plugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup_plugin)
            .add_plugin(extraction::Witcher3ExtractPlugin);
    }
}
// ----------------------------------------------------------------------------
impl Witcher3Config {
    // ------------------------------------------------------------------------
    pub fn new<P: Into<PathBuf>>(game_dir: P) -> Self {
        Self {
            game_dir: game_dir.into(),
            terrain: terrain::TerrainConfig::default(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_terrain_extraction_path<P: Into<PathBuf>>(mut self, path: P) -> Self {
        self.terrain.terrain_dir = path.into();
        self
    }
    // ------------------------------------------------------------------------
    pub fn set_texture_extraction_path<P: Into<PathBuf>>(mut self, path: P) -> Self {
        self.terrain.texture_dir = path.into();
        self
    }
    // ------------------------------------------------------------------------
    pub fn validate(mut self) -> Result<Self, String> {
        let path = self
            .game_dir
            .canonicalize()
            .map_err(|e| format!("failed to expand to absolute path: {}", e))?;
        let executable = path.join(PathBuf::from("bin/x64/witcher3.exe"));

        if executable.is_file() {
            self.game_dir = path;
        } else {
            return Err(format!(
                "could not find witcher3 executable at: {}",
                executable.display()
            ));
        }

        self.terrain = self.terrain.validate()?;
        Ok(self)
    }
    // ------------------------------------------------------------------------
    pub fn check_available_hub_data(&mut self) {
        self.terrain.check_available_hub_data()
    }
    // ------------------------------------------------------------------------
    pub fn terrain(&self) -> &terrain::TerrainConfig {
        &self.terrain
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// systems
// ----------------------------------------------------------------------------
fn setup_plugin(mut conf: ResMut<Witcher3Config>) {
    conf.check_available_hub_data();
}
// ----------------------------------------------------------------------------

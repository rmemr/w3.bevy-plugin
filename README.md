# Witcher 3 Plugin for Witcher 3 Texturing Editor

Bevy plugin for w3 texturing editor to provide and extract data from game installation.

## Contributing

First: thank you for your interest! There are many ways to contribute. You can write bug reports, create pull requests to fix bugs or add new features or write documentation.

Please make sure your pull requests:
  * reference a ticket, if you want to add a new feature please make a ticket first and outline what you are trying to add
  * is formatted with rustfmt
  * compiles with the current main branch

If you have questions, you can find me on the [radish modding tools discord server][radishtools-discord].

## License

All code for the editor in this repository is licensed under:

* GPL-3.0 License ([LICENSE-GPL3](LICENSE-GPL3) or [https://opensource.org/licenses/GPL-3.0](https://opensource.org/licenses/GPL-3.0))

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be licensed as above, without any additional terms or conditions.

[radishtools-discord]:       https://discord.gg/R7Jpzfv

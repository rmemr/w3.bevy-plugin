// ----------------------------------------------------------------------------
use std::time::Duration;

use bevy::{
    app::{AppExit, RunMode, ScheduleRunnerSettings},
    log::LogPlugin,
    prelude::*,
};
use bevy_w3 as witcher3;

use witcher3::{ExtractionProgress, ExtractionRequest, Witcher3HubId};
// ----------------------------------------------------------------------------
#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
enum AppState {
    Main,
}
// ----------------------------------------------------------------------------
fn main() {
    App::new()
        .insert_resource(ScheduleRunnerSettings {
            run_mode: RunMode::Loop {
                wait: Some(Duration::from_millis(250)),
            },
        })
        .add_plugins(MinimalPlugins)
        .add_plugin(LogPlugin::default())
        .add_plugin(witcher3::Witcher3Plugin)
        .insert_resource(
            witcher3::Witcher3Config::new(".")
                .set_terrain_extraction_path("_test-data_/terrain/")
                .set_texture_extraction_path("_test-data_/w3.textures/")
                .validate()
                .expect("witcher3 plugin config"),
        )
        .add_state(AppState::Main)
        .add_startup_system(setup_terrain_extraction_requests)
        // --- data extracton
        .add_system_set(witcher3::Witcher3Plugin::extract_data(AppState::Main))
        // --- end
        .add_system(watch_extraction)
        .run();
}
// ----------------------------------------------------------------------------
fn setup_terrain_extraction_requests(mut commands: Commands) {
    use ExtractionRequest::TerrainData;
    use Witcher3HubId::*;

    let requests = vec![
        TerrainData(Prologue),
        TerrainData(PrologueWinter),
        TerrainData(Novigrad),
        TerrainData(Skellige),
        TerrainData(KaerMorhen),
        TerrainData(Vizima),
        TerrainData(IsleOfMist),
        TerrainData(SpiralSnowValley),
        TerrainData(Bob),
    ];

    commands.insert_resource(requests);
}
// ----------------------------------------------------------------------------
fn watch_extraction(
    mut events: EventReader<ExtractionProgress>,
    mut app_exit: EventWriter<AppExit>,
) {
    use ExtractionProgress::*;

    for event in events.iter() {
        info!("extraction progress: {}", event.caption());

        match event {
            ExtractionFinished | ProgressTrackingCancel => {
                app_exit.send(AppExit);
            }
            _ => {}
        }
    }
}
// ----------------------------------------------------------------------------
trait Caption {
    fn caption(&self) -> String;
}
// ----------------------------------------------------------------------------
impl Caption for ExtractionProgress {
    fn caption(&self) -> String {
        match self {
            ExtractionProgress::ProgressTrackingStart(caption, _) => {
                format!("{} started...", caption)
            }
            ExtractionProgress::ProgressTrackingCancel => "canceled.".to_string(),
            ExtractionProgress::ProgressTrackingUpdate(state) => match state.task_progress_msg() {
                Some(msg) => msg,
                None => {
                    format!(
                        "{}...{}%",
                        state.request,
                        (state.request_progress() * 100.0).trunc()
                    )
                }
            },
            ExtractionProgress::ExtractionFinished => "finished.".to_string(),
        }
    }
}
// ----------------------------------------------------------------------------
